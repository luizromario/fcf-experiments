-- WIP

function make_connector()
    local connector = {}

    local value = nil
    local informant = nil
    local constraints = {}

    function connector.has_val()
        if informant then return true else return false end
    end

    function connector.val()
        return value
    end

    function connector.set(new_value, setter)
        if not connector.has_val() then
            value = new_value
            informant = setter
            for _, c in ipairs(constraints) do
                if c ~= setter then c.i_have_val() end
            end
        elseif value ~= new_value then
            error("Contradiction", value, new_value)
        end
    end

    function connector.forget(retractor)
        if retractor == informant then
            informant = nil
            for _, c in ipairs(constraints) do
                if c ~= retractor then c.i_lost_val() end
            end
        end
    end

    function connector.connect(new_constraint)
        for _, c in ipairs(constraints) do
            if new_constraint == c then return end
        end

        table.insert(constraints, new_constraint)

        if connector.has_val() then
            new_constraint.i_have_val()
        end
    end

    return connector
end

function adder(a1, a2, sum)
    local adder = {}

    function adder.i_have_val()
        if a1.has_val() and a2.has_val() then
            sum.set(a1.val() + a2.val(), adder)
        elseif a1.has_val() and sum.has_val() then
            a2.set(sum.val() - a1.val(), adder)
        elseif a2.has_val() and sum.has_val() then
            a1.set(sum.val() - a2.val(), adder)
        end
    end

    function adder.i_lost_val()
        sum.forget(adder)
        a1.forget(adder)
        a2.forget(adder)
        adder.i_have_val()
    end

    a1.connect(adder)
    a2.connect(adder)
    sum.connect(adder)

    return adder
end

function multiplier(m1, m2, product)
    local multi = {}

    function multi.i_have_val()
        if (m1.has_val() and m2.val() == 0) or
           (m2.has_val() and m2.val() == 0) then
            product.set(0, multi)
        elseif m1.has_val() and m2.has_val() then
            product.set(m1.val() * m2.val(), multi)
        elseif m1.has_val() and product.has_val() then
            m2.set(product.val() / m1.val(), multi)
        elseif m2.has_val() and product.has_val() then
            m1.set(product.val() / m2.val(), multi)
        end
    end

    function multi.i_lost_val()
        product.forget(multi)
        m1.forget(multi)
        m2.forget(multi)
        multi.i_have_val()
    end

    m1.connect(multi)
    m2.connect(multi)
    product.connect(multi)

    return multi
end

function constant(value, connector)
    local const = {}

    connector.connect(const)
    connector.set(value, const)

    return const
end

function probe(name, connector)
    local function print_probe(value)
        print("Probe. " .. name .. " = " .. tostring(value))
    end

    local probe = {}

    function probe.i_have_val()
        print_probe(connector.val())
    end

    function probe.i_lost_val()
        print_probe("?")
    end

    connector.connect(probe)

    return probe
end

function celsius_fahrenheit_connector(c, f)
    local u = make_connector()
    local v = make_connector()
    local w = make_connector()
    local x = make_connector()
    local y = make_connector()

    multiplier(c, w, u)
    multiplier(v, x, u)
    adder(v, y, f)
    constant(9, w)
    constant(5, x)
    constant(32, y)
end