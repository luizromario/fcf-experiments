-- bad Lua counter factory
function mk_counter()
    count = 0
    return function()
        count = count + 1
        return count
    end
end

c1 = mk_counter()
c2 = mk_counter()

print(c1())
print(c1())
print(c1())
print(c2())
print(c2())
print(c2())