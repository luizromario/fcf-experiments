local function makecounter()
    local count = 0

    return
        function() count = count + 1 end,
        function() return count end
end

local c1_inc, c1_res = makecounter()

c1_inc()
c1_inc()
c1_inc()
print(c1_res())
print(c1_res())

local c2_inc, c2_res = makecounter()
local c3_inc, c3_res = makecounter()

c2_inc()
c2_inc()
c2_inc()
c1_inc()
print(c1_res())
print(c2_res())
print(c3_res())