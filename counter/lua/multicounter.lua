local function makecounters()
    local count = 0
    local counters = {}
    for i = 1, 10 do
        counters[i] = function()
            count = count + i
            i = i + 1
            return count
        end
    end

    return counters
end

local counters = makecounters()

print(counters[1]())
print(counters[1]())
print(counters[2]())
print(counters[2]())
print(counters[9]())
print(counters[1]())