local function make_counter()
    local c = 0
    return function()
        c = c + 1
        return c
    end
end

local c1, c2 = make_counter(), make_counter()

print(c1())
print(c1())
print(c1())
print(c2())
print(c2())
print(c1())
print(c2())
print(c2())
print(c1())