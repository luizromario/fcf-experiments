(define (make-counter)
    (let ((count 0))
        (cons
            (lambda () (set! count (+ count 1)))
            (lambda () count))))

(let (
    (c1 (make-counter))
    (c2 (make-counter))
    (c3 (make-counter)))

    ((car c1))
    ((car c1))
    ((car c1))
    (display ((cdr c1))) (newline)
    (display ((cdr c1))) (newline)

    ((car c2))
    ((car c2))
    ((car c2))
    ((car c1))
    (display ((cdr c1))) (newline)
    (display ((cdr c2))) (newline)
    (display ((cdr c3))) (newline)
)