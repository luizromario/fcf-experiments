(define (make-counters)
    (let (
        (count 0)
        (counters '()))

        (define (loop i)
            (if (>= i 10) '()
                (begin
                    (set! counters (append counters (list (lambda ()
                        (set! count (+ count i))
                        (set! i (+ i 1))
                        count))))
                    (loop (+ i 1)))))
        (loop 0)

        counters))

(let ((counters (make-counters)))
    (display ((list-ref counters 0))) (newline)
    (display ((list-ref counters 0))) (newline)
    (display ((list-ref counters 0))) (newline)
    (display ((list-ref counters 1))) (newline)
    (display ((list-ref counters 1))) (newline)
    (display ((list-ref counters 0))) (newline)
    (display ((list-ref counters 5))) (newline)
    (display ((list-ref counters 5))) (newline)
    (display ((list-ref counters 3))) (newline)
    (display ((list-ref counters 2))) (newline)
    (display ((list-ref counters 3))) (newline)
)