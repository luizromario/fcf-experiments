(define mk-counter (lambda ()
    (define n 0)
    (lambda ()
        (set! n (+ n 1))
        n
    )
))

(define c1 (mk-counter))
(define c2 (mk-counter))

(define displayln (lambda (val)
    (display val) (newline)
))

(displayln (c1))
(displayln (c1))
(displayln (c2))
(displayln (c1))
(displayln (c2))
(displayln (c2))
(displayln (c2))
(displayln (c1))
