package main
mkCounter := func() func() int {
    count := 0
    return func() int {
        count++
        return count
    }
}

func main() {
    c := mkCounter()
}