package main

import "fmt"

func makeCounter() (func(), func() int) {
    count := 0
    return func() { count++; },
           func() int { return count; }
}

func main() {
    c1Inc, c1Res := makeCounter()

    c1Inc()
    c1Inc()
    c1Inc()
    fmt.Println(c1Res())
    fmt.Println(c1Res())

    c2Inc, c2Res := makeCounter()
    _, c3Res := makeCounter()

    c2Inc()
    c2Inc()
    c2Inc()
    c1Inc()
    fmt.Println(c1Res())
    fmt.Println(c2Res())
    fmt.Println(c3Res())
}