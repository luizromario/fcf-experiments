package main

import "fmt"

func mkCounter() func() int {
	count := 0
	return func() int {
		count++
		return count
	}
}

func main() {
	c1 := mkCounter()
	c2 := mkCounter()
	
	fmt.Println(c1())
	fmt.Println(c2())
	fmt.Println(c2())
	fmt.Println(c1())
	fmt.Println(c2())
	fmt.Println(c1())
}
