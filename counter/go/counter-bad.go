package main
func mkCounter() func() int {
    count := 0
    func counter() int {
        count++
        return count
    }
    return counter
}