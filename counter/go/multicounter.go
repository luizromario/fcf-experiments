package main

import "fmt"

func makeCounters() [10]func() int {
    count := 0
    var counters [10]func() int

    for cur := 0; cur < 10; cur++ {
        i := cur
        counters[i] = func() int {
            count += i
            i++
            return count
        }
    }

    return counters
}

func main() {
    counters := makeCounters()

    fmt.Println(counters[0]())
    fmt.Println(counters[0]())
    fmt.Println(counters[0]())
    fmt.Println(counters[1]())
    fmt.Println(counters[1]())
    fmt.Println(counters[0]())
    fmt.Println(counters[5]())
    fmt.Println(counters[5]())
    fmt.Println(counters[3]())
    fmt.Println(counters[2]())
    fmt.Println(counters[3]())
}