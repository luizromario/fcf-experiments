// Boxed result, dynamic dispatch
// Before 1.26
fn mk_counter1() -> Box<FnMut() -> u64> {
    let mut count = 0u64;
    
    Box::new(move || -> u64 {
        count += 1;
        count
    })
}

// Unboxed result, static dispatch
// From 1.26 onwards
fn mk_counter2() -> impl FnMut() -> u64 {
    let mut count = 0u64;
    
    move || -> u64 {
        count += 1;
        count
    }
}

fn main() {
    println!("Hello, world!");
    
    let mut c1 = mk_counter1();
    let mut c2 = mk_counter1();
    
    println!("{}", c1());
    println!("{}", c1());
    println!("{}", c2());
    println!("{}", c2());
    println!("{}", c1());
    println!("{}", c2());
    
    let mut c3 = mk_counter2();
    let mut c4 = mk_counter2();
    
    println!("{}", c3());
    println!("{}", c4());
    println!("{}", c4());
    println!("{}", c3());
}
