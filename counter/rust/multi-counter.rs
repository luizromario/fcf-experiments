use std::cell::Cell;
use std::rc::Rc;

fn mk_counter() -> Vec<Box<FnMut() -> u64>> {
    let count = Rc::new(Cell::new(0));
    // It is necessary to explicitly define the type inside the
    // Vec<Box<_>> here because otherwise we'd have a vector of
    // closures of the type of this one specific anonymous function,
    // not an array of trait objects, making it impossible to annotate
    // the declaration -- after all, closure types are not expressible

    // In other words: it seems that each closure expression has an
    // unique type, but all instances of a given closure expression
    // have the same type
    let mut res: Vec<Box<FnMut() -> u64>> = Vec::new();

    for mut i in 0..9 {
        res.push(Box::new({
            let count = count.clone();
            move || {
                count.set(count.get() + i);
                i = i + 1;
                count.get()
            }
        }))
    }

    res
}

fn main() {
    let mut counters = mk_counter();

    println!("{}", (&mut counters[0])());
    println!("{}", (&mut counters[0])());
    println!("{}", (&mut counters[0])());
    println!("{}", (&mut counters[1])());
    println!("{}", (&mut counters[1])());
    println!("{}", (&mut counters[0])());
    println!("{}", (&mut counters[5])());
    println!("{}", (&mut counters[5])());
    println!("{}", (&mut counters[3])());
    println!("{}", (&mut counters[2])());
    println!("{}", (&mut counters[3])());
}
