use std::cell::Cell;
use std::rc::Rc;

fn mk_counter() -> (Box<Fn()>, Box<Fn() -> u64>) {
    let count = Rc::new(Cell::new(0));

    (
        Box::new({
            let count = count.clone();
            move || count.set(count.get() + 1)
        }),
        Box::new({
            let count = count.clone();
            move || count.get()
        })
    )
}

fn main() {
    let (c1_inc, c1_res) = mk_counter();

    c1_inc();
    c1_inc();
    c1_inc();
    println!("{}", c1_res());
    println!("{}", c1_res());

    let (c2_inc, c2_res) = mk_counter();
    let (_, c3_res) = mk_counter();

    c2_inc();
    c2_inc();
    c2_inc();
    c1_inc();
    println!("{}", c1_res());
    println!("{}", c2_res());
    println!("{}", c3_res());
}
