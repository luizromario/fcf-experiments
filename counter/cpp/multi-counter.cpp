#include <functional>
#include <iostream>
#include <memory>
#include <vector>

auto makeCounters()
{
    auto count = std::make_shared<int>(0);
    std::vector<std::function<int()>> counters;
    for (int i = 0; i < 10; ++i)
        counters.push_back([=]() mutable {
            *count += i;
            i++;
            return *count;
        });

    return counters;
}

int main()
{
    auto counters = makeCounters();

    std::cout << counters[0]() << "\n";
    std::cout << counters[0]() << "\n";
    std::cout << counters[0]() << "\n";
    std::cout << counters[1]() << "\n";
    std::cout << counters[1]() << "\n";
    std::cout << counters[0]() << "\n";
    std::cout << counters[5]() << "\n";
    std::cout << counters[5]() << "\n";
    std::cout << counters[3]() << "\n";
    std::cout << counters[2]() << "\n";
    std::cout << counters[3]() << "\n";
}
