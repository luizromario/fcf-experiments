#include <iostream>
#include <memory>
#include <utility>

auto makeCounter()
{
    auto count = std::make_shared<int>(0);

    return std::make_pair(
        [=] { (*count)++; },
        [=] { return *count; }
    );
}

int main()
{
    auto c1 = makeCounter();

    c1.first();
    c1.first();
    c1.first();
    std::cout << c1.second() << "\n";
    std::cout << c1.second() << "\n";

    auto c2 = makeCounter();
    auto c3 = makeCounter();

    c2.first();
    c2.first();
    c2.first();
    c1.first();
    std::cout << c1.second() << "\n";
    std::cout << c2.second() << "\n";
    std::cout << c3.second() << "\n";
}
