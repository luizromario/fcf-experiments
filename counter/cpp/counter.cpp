#include <iostream>
#include <memory>

class counter
{
    int count = 0;

public:
    int operator()() { return count++; }
};

auto mk_counter1()
{
    auto count = std::make_shared<int>(0);

    return [=] {
        return (*count)++;
    };
}

auto mk_counter2()
{
    auto count = std::make_unique<int>(0);
    
    return [count(move(count))] {
        return (*count)++;
    };
}

auto mk_counter3()
{
    int c = 0;
    return [=]() mutable {
        return c++;
    };
}

int main()
{
    counter c1;
    counter c2;

    std::cout << c1() << "\n";
    std::cout << c2() << "\n";
    std::cout << c2() << "\n";
    std::cout << c2() << "\n";
    std::cout << c1() << "\n";
    std::cout << c1() << "\n";

    std::cout << "---\n";

    auto c3 = mk_counter1();
    auto c4 = mk_counter1();

    std::cout << c3() << "\n";
    std::cout << c4() << "\n";
    std::cout << c4() << "\n";
    std::cout << c4() << "\n";
    std::cout << c3() << "\n";
    std::cout << c3() << "\n";

    std::cout << "---\n";

    auto c5 = mk_counter2();
    auto c6 = mk_counter2();

    std::cout << c5() << "\n";
    std::cout << c6() << "\n";
    std::cout << c6() << "\n";
    std::cout << c6() << "\n";
    std::cout << c5() << "\n";
    std::cout << c5() << "\n";

    auto c7 = mk_counter3();
    auto c8 = mk_counter3();

    std::cout << "---\n";

    std::cout << c7() << "\n";
    std::cout << c8() << "\n";
    std::cout << c8() << "\n";
    std::cout << c8() << "\n";
    std::cout << c7() << "\n";
    std::cout << c7() << "\n";
    
    return 0;
}
