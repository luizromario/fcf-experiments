using System;

delegate int Counter();

public class Program
{
    static Counter[] MakeCounters()
    {
        int count = 0;
        Counter[] counters = new Counter[10];
        // it seems the same "cur" is used throughout the loop
        for (int cur = 0; cur < 10; ++cur)
        {
            int i = cur;
            counters[i] = () => {
                count = count + i;
                i = i + 1;
                return count;
            };
        }

        return counters;
    }

    public static void Main()
    {
        var counters = MakeCounters();

        Console.WriteLine(counters[0]());
        Console.WriteLine(counters[0]());
        Console.WriteLine(counters[0]());
        Console.WriteLine(counters[1]());
        Console.WriteLine(counters[1]());
        Console.WriteLine(counters[0]());
        Console.WriteLine(counters[5]());
        Console.WriteLine(counters[5]());
        Console.WriteLine(counters[3]());
        Console.WriteLine(counters[2]());
        Console.WriteLine(counters[3]());
    }
}