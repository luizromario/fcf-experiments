using System;

delegate int Counter();

// anon methods and lambda exprs are apparentlly purely syntactic
// and do not have a type of their own

class CounterObj
{
	int count;
	
	public int Count()
	{
		return count++;
	}
}

public class Program
{
	static Counter MakeCounter1()
	{
		CounterObj cobj = new CounterObj();
		return cobj.Count;
	}
	
	static Counter MakeCounter2()
	{
		int count = 0;
		Counter c = delegate()
		{
			return count++;
		};
		
		// Cannot assign anonymous method to an implictly-typed variable
		// var del = delegate(){return count++;};
		var del = (Counter) delegate(){return count++;};
		
		return c;
	}
	
	static Counter MakeCounter3()
	{
		int count = 0;
		// Cannot assign lambda expression to an implicitly-typd variable
		// var anon = () => count++;
		var anon = (Counter) (() => count++);
		return () => count++;
	}
	
	public static void Main()
	{
		Console.WriteLine("Hello Porra");
		
		Counter c1 = MakeCounter1();
		
		Console.WriteLine(c1());
		Console.WriteLine(c1());
		Console.WriteLine(c1());
		Console.WriteLine(c1());
		
		Console.WriteLine("---");
		
		Counter c2 = MakeCounter2();
		Counter c3 = MakeCounter2();
		
		Console.WriteLine(c2());
		Console.WriteLine(c3());
		Console.WriteLine(c2());
		Console.WriteLine(c3());
		Console.WriteLine(c2());
		
		Console.WriteLine("---");
		
		Counter c4 = MakeCounter3();
		Counter c5 = MakeCounter3();
		
		Console.WriteLine(c4());
		Console.WriteLine(c5());
		Console.WriteLine(c5());
		Console.WriteLine(c5());
		Console.WriteLine(c4());
		Console.WriteLine(c4());
		Console.WriteLine(c4());
	}
}
