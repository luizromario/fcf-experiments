using System;

delegate void Incrementer();
delegate int Result();

public class Program
{
    static Tuple<Incrementer, Result> MakeCounter()
    {
        int count = 0;
        return new Tuple<Incrementer, Result>(
            () => count = count + 1,
            () => count
        );
    }

    public static void Main()
    {
        var c1 = MakeCounter();

        c1.Item1();
        c1.Item1();
        c1.Item1();
        Console.WriteLine(c1.Item2());
        Console.WriteLine(c1.Item2());

        var c2 = MakeCounter();
        var c3 = MakeCounter();

        c2.Item1();
        c2.Item1();
        c2.Item1();
        c1.Item1();
        Console.WriteLine(c1.Item2());
        Console.WriteLine(c2.Item2());
        Console.WriteLine(c3.Item2());
    }
}