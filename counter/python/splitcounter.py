def make_counter():
    count = 0
    def increment():
        nonlocal count
        count += 1
    def result():
        return count

    return increment, result

c1_inc, c1_res = make_counter()

c1_inc()
c1_inc()
c1_inc()
print(c1_res())
print(c1_res())

c2_inc, c2_res = make_counter()
c3_inc, c3_res = make_counter()

c2_inc()
c2_inc()
c2_inc()
c1_inc()
print(c1_res())
print(c2_res())
print(c3_res())