def mk_counter():
    count = 0
    return lambda:
        count += 1
        return count

c = mk_counter()
print(c())
print(c())
print(c())