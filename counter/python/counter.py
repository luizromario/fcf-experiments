def mk_counter():
    class CountData:
        n = -1

        def count(self):
            self.n += 1
            return self.n

    d = CountData()

    return lambda: d.count()

c1 = mk_counter()
c2 = mk_counter()

print(c1())
print(c2())
print(c2())
print(c2())
print(c1())
print(c1())
