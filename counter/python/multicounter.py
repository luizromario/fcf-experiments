def make_counters():
    count = 0
    counters = []
    for i in range(0, 10):
        def counter():
            nonlocal count, i
            count = count + i
            i = i + 1
            return count
        counters.append(counter)

    return counters

counters = make_counters()

print(counters[0]())  # wip (should print 0, prints 9)