def mk_counter_bad():
    count = 0
    def counter():
        # local variable referenced before assignment
        count += 1
        return count

    return counter

c1 = mk_counter_bad()
c2 = mk_counter_bad()

print(c1())
print(c2())
print(c2())
print(c2())
print(c1())
print(c1())

def mk_counter1():
    count = -1
    def incr(n): n += 1

    return lambda: [incr(count), count][1]
