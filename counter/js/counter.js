function makeCounter() {
    let n = 0;
    // In JavaScript, lambdas are called arrow functions
    return () => n++;
}

let c1 = makeCounter();
let c2 = makeCounter();

console.log(c1());
console.log(c1());
console.log(c2());
console.log(c1());
console.log(c1());
console.log(c2());
console.log(c1());
console.log(c1());
console.log(c2());
console.log(c1());
console.log(c1());
console.log(c2());
