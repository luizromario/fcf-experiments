function makeCounter() {
    let n = 0;
    return {
        increment: () => n++,
        result: () => n
    }
}

let c1 = makeCounter();

c1.increment();
c1.increment();
c1.increment();
console.log(c1.result());

let c2 = makeCounter();
let c3 = makeCounter();

c2.increment();
c2.increment();
c2.increment();
c1.increment();
console.log(c1.result());
console.log(c2.result());
console.log(c3.result());
