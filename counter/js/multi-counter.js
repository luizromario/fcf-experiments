function makeCounters() {
    let count = 0;
    let counters = [];
    // since var ties a variable to its function scope, not to its local scope,
    // this would not work
    // for (var i = 0; i < 10; i++)
    for (let i = 0; i < 10; i++)
        counters.push(() => {
            count += i;
            i++;
            return count;
        });

    return counters;
}

let counters = makeCounters();

console.log(counters[0]());
console.log(counters[0]());
console.log(counters[0]());
console.log(counters[1]());
console.log(counters[1]());
console.log(counters[0]());
console.log(counters[5]());
console.log(counters[5]());
console.log(counters[3]());
console.log(counters[2]());
console.log(counters[3]());
