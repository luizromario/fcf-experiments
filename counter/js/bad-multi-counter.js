function makeCounters() {
    var count = 0;
    var counters = [];

    // var ties a variable to its function scope
    // so this fails
    for (var i = 0; i < 10; ++i)
        counters.push(() => {
            count += i;
            i++;
            return count;
        });

    return counters;
}



var counters = makeCounters();

console.log(counters[0]());
console.log(counters[0]());
console.log(counters[0]());
console.log(counters[1]());
console.log(counters[1]());
console.log(counters[0]());
console.log(counters[5]());
console.log(counters[5]());
console.log(counters[3]());
console.log(counters[2]());
console.log(counters[3]());
