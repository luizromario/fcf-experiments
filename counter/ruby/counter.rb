def mkCounter
    c = 0
    proc do
        c += 1
        # return c  # counter.rb:5:in `block in mkCounter': unexpected return (LocalJumpError)
        c
    end
end

def mkCounter2
    c = 0
    lambda do
        c += 1
        return c  # ok
    end
end

c1 = mkCounter
c2 = mkCounter
c3 = mkCounter2

puts c1.call
puts c1.call
puts c1.call
puts c2.call
puts c2.call
puts c3.call
puts c3.call
# puts c3.call(10)  # counter.rb:12:in `block in mkCounter2': wrong number of arguments (given 1, expected 0) (ArgumentError)
puts c2.call(10)
puts c1.call
puts c2.call