def make_counters
    count = 0
    counters = []
    (0..9).each do |i|
        counters << Proc.new do
            count += i
            i += 1
            count
        end
    end

    counters
end

counters = make_counters

puts counters[0].call
puts counters[0].call
puts counters[0].call
puts counters[1].call
puts counters[1].call
puts counters[0].call
puts counters[5].call
puts counters[5].call
puts counters[3].call
puts counters[2].call
puts counters[3].call