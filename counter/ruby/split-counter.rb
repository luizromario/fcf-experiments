def make_counter
    count = 0
    [Proc.new do
        count += 1
        nil  # force it to return nil
    end,
    Proc.new do
        count
    end]
end

c1 = make_counter

c1[0].call
c1[0].call
c1[0].call
puts c1[1].call
puts c1[1].call

c2 = make_counter
c3 = make_counter

c2[0].call
c2[0].call
c2[0].call
c1[0].call
puts c1[1].call
puts c2[1].call
puts c3[1].call