#include <Foundation/Foundation.h>

typedef int (^Counter)(void);

Counter mkCounter()
{
    __block int c = 0;
    // this block is local to the function's scope, can't be returned
    // return ^{ return c++; }
    // Block_copy turns the block into an object
    return Block_copy(^{ return c++; });
}

int main(void)
{
    Counter c1 = mkCounter();
    Counter c2 = mkCounter();

    NSLog(@"%d", c1());
    NSLog(@"%d", c1());
    NSLog(@"%d", c2());
    NSLog(@"%d", c2());
    NSLog(@"%d", c1());
    NSLog(@"%d", c2());
    NSLog(@"%d", c2());
    NSLog(@"%d", c1());
    NSLog(@"%d", c1());

    Block_release(c2);
    Block_release(c1);
}