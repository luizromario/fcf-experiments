public class CounterSplit {
    interface Incrementer {
        public void increment();
    }

    interface GetResult {
        public int getResult();
    }

    static class Counter {
        public Counter(Incrementer i, GetResult r) {
            inc = i;
            res = r;
        }

        public Incrementer inc;
        public GetResult res;
    }

    static Counter makeCounter() {
        // value has to be final in order to be usable inside a lambda
        // int c = 0;

        // even though Integers are allocated in the heap, Integer is
        // an immutable class
        // Integer c = new Integer(0);

        class CountData {
            int c = 0;
        }

        CountData data = new CountData();

        return new Counter(
            () -> data.c = data.c + 1,
            () -> data.c
        );
    }

    public static void main(String[] args) {
        Counter c1 = makeCounter();
        c1.inc.increment();
        c1.inc.increment();
        c1.inc.increment();
        System.out.println(c1.res.getResult());
        System.out.println(c1.res.getResult());

        Counter c2 = makeCounter();
        Counter c3 = makeCounter();

        c2.inc.increment();
        c2.inc.increment();
        c2.inc.increment();
        c1.inc.increment();
        System.out.println(c1.res.getResult());
        System.out.println(c2.res.getResult());
        System.out.println(c3.res.getResult());
    }
}