public class MyClass {
    interface Counter {
        public int count();
    }

    static Counter mkCounter1() {
        // inner classes can only capture outer variables that are final
        return new Counter() {
            int c = 0;

            public int count() {
                return c++;
            }
        };
    }

    static Counter mkCounter2() {
        class CountData {
            int c = 0;
        }

        final CountData data = new CountData();

        // We're not modifying the value of data (the pointer), so this
        // doesn't violate the final qualifier
        return () -> data.c++;
    }

    public static void main(String args[]) {
        Counter c1 = mkCounter1();
        Counter c2 = mkCounter1();

        System.out.println(c1.count());
        System.out.println(c2.count());
        System.out.println(c2.count());
        System.out.println(c1.count());

        System.out.println("---");

        Counter c3 = mkCounter2();
        Counter c4 = mkCounter2();

        System.out.println(c3.count());
        System.out.println(c3.count());
        System.out.println(c4.count());
        System.out.println(c4.count());
        System.out.println(c4.count());
        System.out.println(c3.count());
    }
}
