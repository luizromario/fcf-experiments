Object subclass: OoCounter [
    |c|

    OoCounter class >> new [
        |ctr|

        ctr := super new.
        ctr init.
        ^ctr
    ]

    init [c := 0]
    count [
        c := c + 1.
        ^c
    ]
]

c1 := OoCounter new.
c2 := OoCounter new.

c1 count printNl.
c1 count printNl.
c1 count printNl.
c2 count printNl.
c2 count printNl.
c1 count printNl.

fnCounter := [
    |c|
    c := 0.

    [
        c := c + 1.
        c
    ]
].

c3 := fnCounter value.
c4 := fnCounter value.

c3 value printNl.
c3 value printNl.
c3 value printNl.
c4 value printNl.
c4 value printNl.
c3 value printNl.
