class Composite
    def initialize(&fst)
        @fst = fst
    end
    
    def with(&snd)
        @snd = snd
        self
    end
    
    def call(arg)
        @fst.call(@snd.call(arg))
    end
end

def compose(&fst)
    Composite.new(&fst)
end

def compose2(f, g)
    proc{|x| f.call(g.call(x))}
end
