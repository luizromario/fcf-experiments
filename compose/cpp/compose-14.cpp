#include <cmath>
#include <iostream>

template <typename F, typename G>
auto compose(F f, G g)
{
    return [=](auto x)
    {
        return f(g(x));
    };
}

int main()
{
    std::cout << compose(sqrt, [](auto x) { return x * x; })(10) << "\n";
}