package main

import("fmt"; "math")

func compose(f func(float64) float64, g func(float64) float64) func(float64) float64 {
    return func(x float64) float64 {
        return f(g(x))
    }
}

func main() {
    fmt.Println(compose(math.Sqrt, func(x float64) float64 { return x * x })(10))

//     // illegal code
//     // cannot convert int -> int functions into float -> float
//     fmt.Println(compose(
//         func(x int) int { return x + 1; },
//         func(y int) int { return y * 2; })(10));

//     // even if you try to manually convert the functions
//     // cannot convert func literal
//     fmt.Println(compose(
//         func(float64) float64(func(x int) int { return x + 1; }),
//         func(float64) float64(func(y int) int { return y * 2; }))(10));

    // even though ints are convertible to floats
    var i int;
    i = 10;
    func(x float64) float64 { return x; }(float64(i));
}
