import math

def compose(f, g):
    return lambda x: f(g(x))

print(compose(math.sqrt, lambda x: x * x)(10))