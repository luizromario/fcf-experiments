class DerivSqr {
    static class Sqr implements Deriv.Function {
        public double eval(double x) {
            return x * x;
        }
    }

    public static void main(String[] args) {
        System.out.println(Deriv.apply(new Sqr(), 10));

        Deriv.Function sqrDeriv = Deriv.get(new Sqr());
        System.out.println(sqrDeriv.eval(10));
    }
}
