class Deriv {
    static double dx = 0.0000001;

    public interface Function {
        double eval(double x);
    }

    public static double apply(Function f, double x) {
        return (f.eval(x + dx) - f.eval(x)) / dx;
    }

    static class GetDeriv implements Function {
        Function f;

        public GetDeriv(Function f) {
            this.f = f;
        }

        public double eval(double x) {
            return apply(f, x);
        }
    }

    public static Function get(Function f) {
        return new GetDeriv(f);
    }
}
