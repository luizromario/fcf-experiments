class DerivSqrLambda {
    public static void main(String[] args) {
        System.out.println(Deriv.apply(x -> x * x, 10));

        Deriv.Function sqrDeriv = Deriv.get(x -> x * x);
        System.out.println(sqrDeriv.eval(10));
    }
}
