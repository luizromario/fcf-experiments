#include <stdio.h>

double deriv(double (*f)(double), double x)
{
    const double small = 0.0000001;
    return (f(x + small) - f(x)) / small;
}

double sqr(double x)
{
    return x * x;
}

int main()
{
    printf("%f\n", deriv(sqr, 10));
}
