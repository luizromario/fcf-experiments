#include <stdio.h>

#define SMALL 0.0000001
#define DERIV(__f, __x) (__f((__x) + SMALL) - __f(__x)) / SMALL

double sqr(double x)
{
    return x * x;
}

int main()
{
    printf("%f\n", DERIV(sqr, 10.0));
}
