local inspect = require 'inspect'

local function takecallback(arr, cb)
    local ret = {}
    for i, el in ipairs(arr) do
        ret[i] = cb(el)
    end
    return ret
end

print(inspect(takecallback({1, 2, 3, 4, 5}, function(n) return tostring(n) end)))
print(inspect(takecallback({10, 20, 30, 40, 50}, function(n) return n / 100 end)))
