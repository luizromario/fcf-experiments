#include <functional>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

template<typename T, typename U>
vector<U> take_callback(const vector<T> &v, function<U(T)> cb)
{
    vector<U> ret;
    for (const auto &el : v)
        ret.push_back(cb(el));
    return ret;
}

template<typename T>
ostream &operator<<(ostream &stream, const vector<T> &v)
{
    stream << "{";
    for (const auto &el : v) {
        stream << el;
        stream << ", ";
    }
    stream << "}";
    
    return stream;
}

int main()
{
    cout << take_callback(
        {1, 2, 3, 4, 5},
        function<string(int)>([](auto n) { return to_string(n); })) << "\n";
    cout << take_callback(
        {10, 20, 30, 40, 50},
        function<float(int)>([](auto n) { return static_cast<float>(n) / 100; })) << "\n";

    return 0;
}
