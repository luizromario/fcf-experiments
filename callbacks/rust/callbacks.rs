fn take_callback<T, U, Cb: Fn(&T) -> U>(v: &Vec<T>, cb: Cb) -> Vec<U> {
    let mut res = Vec::new();
    for el in v.iter() {
        res.push(cb(el));
    }
    res
}

fn main() {
    println!("{:?}", take_callback(&vec![1, 2, 3, 4, 5], |n| n.to_string()));
    println!("{:?}", take_callback(&vec![10, 20, 30, 40, 50], |n| (*n as f32) / 100.0));
}
