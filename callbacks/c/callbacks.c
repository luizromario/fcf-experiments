#include <stddef.h>
#include <stdio.h>

typedef void (*callback_t)(void *, void *);

void take_callback(
    void *vec, size_t len, size_t intypesize,
    callback_t cb, void *output, size_t outtypesize)
{
    for (int i = 0; i < len; ++i) {
        cb(vec, output);
        vec += intypesize;
        output += outtypesize;
    }
}

void to_string_cb(void *intval_p, void *strout)
{
    sprintf((char *) strout, "%d", *(int *) intval_p);
}

void div_by_100_cb(void *intval_p, void *floatout_p)
{
    int intval = *(int *) intval_p;
    *(float *) floatout_p = intval / 100.0;
}

int main()
{
    {
        int arr[] = {1, 2, 3, 4, 5};
        const int arrsize = sizeof(arr) / sizeof(int);
        char sarr_buffer[arrsize * 16];

        take_callback(
            arr, arrsize, sizeof(int),
            to_string_cb, sarr_buffer, sizeof(char) * 16);

        for (int i = 0; i < arrsize * 16; i += 16)
            printf("\"%s\" ", sarr_buffer + i);
        printf("\n");
    }

    {
        int arr[] = {10, 20, 30, 40, 50};
        const int arrsize = sizeof(arr) / sizeof(int);
        float farr[arrsize];

        take_callback(
            arr, arrsize, sizeof(int),
            div_by_100_cb, farr, sizeof(float));

        for (int i = 0; i < arrsize; ++i)
            printf("%f ", farr[i]);
        printf("\n");
    }
}
