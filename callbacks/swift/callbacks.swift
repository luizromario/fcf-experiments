func takeCallback<T, U>(_ vec: [T], _ cb: (T) -> U) -> [U] {
    var res = [U]()
    for el in vec {
        res.append(cb(el))
    }
    return res
}

print(takeCallback([1, 2, 3, 4, 5], { n in String(n) }))
print(takeCallback([10, 20, 30, 40, 50], { n in Float(n) / 100 }))
