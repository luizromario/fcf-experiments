#include <stdio.h>
#include <stdlib.h>

typedef void (*slot_t)(int);

typedef struct _slot_list_t {
    slot_t slot;
    struct _slot_list_t *next;
} slot_list_t;

typedef struct {
    slot_list_t *slots;
} signal_t;

void signal_init(signal_t *signal)
{
    signal->slots = NULL;
}

void signal_destruct(signal_t *signal)
{
    slot_list_t *slots = signal->slots;
    if (!slots)
        return;

    do {
        slot_list_t *next = slots->next;
        free(slots);
        slots = next;
    } while (slots->next);
}

slot_list_t *slot_list_end(signal_t *signal)
{
    slot_list_t *slots = signal->slots;
    if (!slots)
        return NULL;

    for (; slots->next; slots = slots->next);

    return slots;
}

void add_slot(slot_list_t **slot_list, slot_t slot)
{
    *slot_list = malloc(sizeof(slot_list_t));
    (*slot_list)->slot = slot;
    (*slot_list)->next = NULL;

    return;
}

void connect(signal_t *signal, slot_t slot)
{
    slot_list_t *end = slot_list_end(signal);
    if (!end) {
        add_slot(&signal->slots, slot);
        return;
    }

    add_slot(&end->next, slot);
    return;
}

void emit(signal_t *signal, int arg)
{
    slot_list_t *slots = signal->slots;
    if (!slots)
        return;

    for (; slots; slots = slots->next)
        slots->slot(arg);
}

signal_t value_changed;

void changed_to(int value)
{
    printf("changed to %d\n", value);
}

void changed_to_gt100(int _)
{
    printf("  (changed to >100 in the past)\n");
}

void connect_to_changed_to_gt100(int value)
{
    if (value > 100)
        connect(&value_changed, changed_to_gt100);
}

int main()
{
    signal_init(&value_changed);

    connect(&value_changed, changed_to);

    emit(&value_changed, 10);
    emit(&value_changed, 20);

    connect(&value_changed, connect_to_changed_to_gt100);
    
    emit(&value_changed, 30);
    emit(&value_changed, 40);
    emit(&value_changed, 200);
    emit(&value_changed, 50);
    emit(&value_changed, 60);
    
    signal_destruct(&value_changed);
    
    return 0;
}
