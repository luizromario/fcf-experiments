local Signal = require 'signal'

function remaining_label()
    local text = ''
    local rem = 0
    local on_value_changed = Signal()

    on_value_changed:connect(function(value) rem = value end)
    on_value_changed:connect(function(value)
        text = value .. 'MB remaining'
    end)

    return {
        text = function() return text end,
        remaining = function() return rem end,
        set_remaining = function(value)
            return on_value_changed:emit(value)
        end,
        on_remaining_changed = function(f)
            on_value_changed:connect(f)
        end
    }
end