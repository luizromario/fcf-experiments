local Signal = require 'signal'

local value_changed = Signal()

value_changed:connect(function(value)
    print('changed to ' .. value)
end)

value_changed:emit(10)
value_changed:emit(20)

-- WIP
value_changed:connect(function(value)
    if value > 100 then
        value_changed:connect(function()
            print('  (value has changed to >100 in the past)')
        end)
    end
end)

value_changed:emit(30)
value_changed:emit(40)
value_changed:emit(200)
value_changed:emit(50)
value_changed:emit(60)

return mksignal
