local __signal = {}

function __signal.connect(self, slot)
    table.insert(self.slots, slot)
end

function __signal.emit(self, ...)
    for _, slot in ipairs(self.slots) do
        slot(...)
    end
end

local function Signal()
    local signal = {}
    signal.slots = {}
    setmetatable(signal, {__index = __signal})

    return signal
end

return Signal
