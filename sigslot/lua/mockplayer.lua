local Signal = require 'signal'

local openfile_clicked = Signal()
local filedialog = {
    ok = Signal(),
    cancel = Signal()
}
local playvideo_clicked = Signal()
local volumechanged = Signal()
local highvolumedialog = {
    ok = Signal(),
    cancel = Signal()
}

openfile_clicked:connect(function()
    print 'Open file (type in "cancel" to cancel)'

    local res = io.read()
    if res == 'cancel' then
        filedialog.cancel:emit()
    else
        filedialog.ok:emit(res)
    end
end)

filedialog.cancel:connect(function()
    print 'User cancelled opening'
end)

filedialog.ok:connect(function(filename)
    print('Opened ' .. filename)

    playvideo_clicked:connect(function()
        print('Playing ' .. filename)

        volumechanged:connect(function(volume)
            print('Volume changed to ' .. volume)

            if volume > 100 then
                print 'Volume is too loud. Increase it anyway?'

                local res = io.read()
                if res == 'yes' then
                    highvolumedialog.ok:emit(volume)
                else
                    highvolumedialog.cancel:emit()
                end
            end
        end)
    end)
end)

highvolumedialog.cancel:connect(function()
    volumechanged:emit(100)
end)

openfile_clicked:emit()  -- this openfile will be cancelled
playvideo_clicked:emit() -- does nothing
openfile_clicked:emit()  -- this openfile will be accepted
playvideo_clicked:emit()
volumechanged:emit(80)
volumechanged:emit(110)  -- reject this volume change
volumechanged:emit(110)  -- accept this one
