using System;

class ValueChanged
{
    static void Main(string[] args)
    {
        var valueChanged = new Signal<int>();

        valueChanged.Connect(value =>
            Console.WriteLine("changed to {0}", value));

        valueChanged.Emit(10);
        valueChanged.Emit(20);

        valueChanged.Connect(value => {
            if (value > 100)
            {
                valueChanged.Connect(_ =>
                    Console.WriteLine("  (changed to >100 in the past)"));
            }
        });

        valueChanged.Emit(30);
        valueChanged.Emit(40);
        valueChanged.Emit(200);
        valueChanged.Emit(50);
        valueChanged.Emit(60);
    }
}
