using System.Collections.Generic;

public class Signal<T>
{
    public delegate void slot(T args);

    List<slot> slots = new List<slot>();

    public void Connect(slot s)
    {
        slots.Add(s);
    }

    public void Emit (T args)
    {
        // avoid following error:
        // Collection was modified; enumeration operation may not execute.
        var slots = new List<slot>(this.slots);
        foreach (slot s in slots)
        {
            s(args);
        }
    }
}
