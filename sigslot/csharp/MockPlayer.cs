using System;

class MockPlayer
{
    class Diag<T>
    {
        public Signal<object> cancel = new Signal<object>();
        public Signal<T> ok = new Signal<T>();
    }

    static void Main(string[] args)
    {
        var openFileClicked = new Signal<object>();
        var fileDialog = new Diag<string>();
        var playVideoClicked = new Signal<object>();
        var volumeChanged = new Signal<int>();
        var highVolumeDialog = new Diag<int>();

        openFileClicked.Connect(_ => {
            Console.WriteLine("Open file (type in \"cancel\" to cancel)");

            var res = Console.ReadLine();
            if (res == "cancel")
                fileDialog.cancel.Emit(null);
            else
                fileDialog.ok.Emit(res);
        });

        fileDialog.cancel.Connect(_ =>
            Console.WriteLine("User cancelled opening"));

        fileDialog.ok.Connect(filename => {
            Console.WriteLine("Opened " + filename);

            playVideoClicked.Connect(_ => {
                Console.WriteLine("Playing " + filename);

                volumeChanged.Connect(volume => {
                    if (volume <= 100) {
                        Console.WriteLine("Volume changed to " + volume);
                    } else {
                        Console.WriteLine(
                            "Volume is too loud. Increase it anyway?");

                        var res = Console.ReadLine();
                        if (res == "yes")
                            highVolumeDialog.ok.Emit(volume);
                        else
                            highVolumeDialog.cancel.Emit(null);
                    }
                });
            });
        });

        highVolumeDialog.cancel.Connect(volume => volumeChanged.Emit(100));
        highVolumeDialog.ok.Connect(volume =>
            Console.WriteLine("Volume changed to " + volume));

        openFileClicked.Emit(null);   // this openfile will be cancelled
        playVideoClicked.Emit(null);  // does nothing
        openFileClicked.Emit(null);   // this openfile will be accepted
        playVideoClicked.Emit(null);
        volumeChanged.Emit(80);
        volumeChanged.Emit(110);      // reject this volume change
        volumeChanged.Emit(110);      // accept this one
    }
}