package main

import "fmt"

type Signal struct {
    Slots []func(int)
}

func (s *Signal) Connect(slot func(int)) {
    s.Slots = append(s.Slots, slot)
}

func (s Signal) Emit(arg int) {
    for _, slot := range s.Slots {
        slot(arg)
    }
}

func main() {
    valueChanged := Signal{}

    valueChanged.Connect(func(value int) {
        fmt.Println("changed to", value)
    })

    valueChanged.Emit(10)
    valueChanged.Emit(20)

    valueChanged.Connect(func(value int) {
        if value > 100 {
            valueChanged.Connect(func(_ int) {
                fmt.Println("  (value changed to >100 in the past)")
            })
        }
    })

    valueChanged.Emit(30)
    valueChanged.Emit(40)
    valueChanged.Emit(200)
    valueChanged.Emit(50)
    valueChanged.Emit(60)
}
