import java.io.Console;

class Diag<T> {
    public Signal<Void> cancel = new Signal<>();
    public Signal<T> ok = new Signal<>();
}

public class MockPlayer<T> {
    public static void main(String args[]) {
        Signal<Void> openfile_clicked = new Signal<>();
        Diag<String> file_dialog = new Diag<>();
        Signal<Void> playvideo_clicked = new Signal<>();
        Signal<Integer> volumechanged = new Signal<>();
        Diag<Integer> highvolume_dialog = new Diag<>();

        Console c = System.console();

        // Java lambdas cannot modify the variables they capture. However,
        // this fact rarely is a problem in most situations, since, most of
        // the time, variables are references, which, even if final, allow
        // the programmer to change the content they're pointing to through
        // methods. This is not a very noticeable limitation for most usecases

        // Even when the programmer gets in a situation where they wish they
        // could modify a captured variable, they can simply create a new
        // class with the single field they want to modify
        openfile_clicked.connect((_u) -> {
            System.out.println("Open file (type in \"cancel\" to cancel)");
            String res = c.readLine();

            if (res.equals("cancel"))
                file_dialog.cancel.emit(null);
            else
                file_dialog.ok.emit(res);
        });

        file_dialog.cancel.connect((_u) ->
            System.out.println("User cancelled opening"));

        file_dialog.ok.connect((filename) -> {
            System.out.println("Opened " + filename);

            playvideo_clicked.connect((_u) -> {
                System.out.println("Playing " + filename);

                volumechanged.connect((volume) -> {
                    if (volume <= 100) {
                        System.out.println(
                            "Volume changed to " + volume.toString());
                    } else {
                        System.out.println(
                            "Volume is too loud. Increase it anyway?");
                        String res = c.readLine();

                        if (res.equals("yes"))
                            highvolume_dialog.ok.emit(volume);
                        else
                            highvolume_dialog.cancel.emit(null);
                    }
                });
            });
        });

        highvolume_dialog.cancel.connect((_u) -> volumechanged.emit(100));
        highvolume_dialog.ok.connect((volume) ->
            System.out.println("Volume changed to " + volume.toString()));

        openfile_clicked.emit(null);   // this openfile will be cancelled
        playvideo_clicked.emit(null);  // does nothing
        openfile_clicked.emit(null);   // this openfile will be accepted
        playvideo_clicked.emit(null);
        volumechanged.emit(80);
        volumechanged.emit(110);       // reject this volume change
        volumechanged.emit(110);       // accept this one
    }
}