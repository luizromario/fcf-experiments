public class ValueChanged {
    public static void main(String[] args) {
        Signal<Integer> valueChanged = new Signal<>();

        valueChanged.connect(value -> System.out.println("changed to " + value));

        valueChanged.emit(10);
        valueChanged.emit(20);

        valueChanged.connect(value -> {
            if (value > 100) {
                valueChanged.connect(v ->
                    System.out.println("  (value changed to >100 in the past)"));
            }
        });

        valueChanged.emit(30);
        valueChanged.emit(40);
        valueChanged.emit(200);
        valueChanged.emit(50);
        valueChanged.emit(60);
    }
}
