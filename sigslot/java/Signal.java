import java.util.*;

public class Signal<T> {
    interface Slot<T> {
        public void call(T arg);
    }

    ArrayList<Slot<T>> _slots = new ArrayList<>();

    public void connect(Slot<T> slot) {
        _slots.add(slot);
    }

    public void emit(T arg) {
        // avoid modifying _slots during loop
        // (prevent ConcurrentModificationException)
        ArrayList<Slot<T>> slots = new ArrayList<>(_slots);
        for (Slot<T> slot : slots)
            slot.call(arg);
    }
}
