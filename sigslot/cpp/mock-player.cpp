#include "signal.hpp"

#include <iostream>
#include <string>

template <typename T>
class diag
{
public:
    signal<> cancel;
    signal<T> ok;
};

using namespace std;

int main()
{
    signal<> openfile_clicked;
    diag<string> filedialog;
    signal<> playvideo_clicked;
    signal<int> volumechanged;
    diag<int> highvolumedialog;

    openfile_clicked.connect([&] {
        cout << "Open file (type in \"cancel\" to cancel)\n";

        string res;
        getline(cin, res);

        if (res == "cancel")
            filedialog.cancel.emit();
        else
            filedialog.ok.emit(res);
    });

    filedialog.cancel.connect([&] {
        cout << "User cancelled opening\n";
    });

    filedialog.ok.connect([&] (auto filename) {
        cout << "Opened " << filename << "\n";

        // I have to pay attention here because filename dies after the end of
        // this slot, so the reference inside of the playvideo slot points will
        // point to a dead variable if I link by reference

        // however, capturing volumechanged, for example, by value, would break
        // the program, because I want to call the actual volumechanged signal,
        // not a copy of it. So, make the default capture by reference and
        // capture only filename by value

        // Also, captures by value are const by default, so the following would
        // fail to compile (because signal::connect() is not const):
        // playvideo_clicked.connect([=] {

        // You still can, though, allow lambdas to modify values capture by
        // value by declaring the lambda with the mutable keyword like so:
        // playvideo_clicked.connect([=] () mutable {
        // but that would lead to problems mentioned earlier
        playvideo_clicked.connect([&, filename] {
            cout << "Playing " << filename << "\n";

            volumechanged.connect([&] (auto volume) {
                cout << "Volume changed to " << volume << "\n";

                if (volume > 100) {
                    cout << "Volume is too loud. Increase it anyway?\n";

                    string res;
                    getline(cin, res);

                    if (res == "yes")
                        highvolumedialog.ok.emit(volume);
                    else
                        highvolumedialog.cancel.emit();
                }
            });
        });
    });

    highvolumedialog.cancel.connect([&] {
        volumechanged.emit(100);
    });

    openfile_clicked.emit();   // this openfile will be cancelled
    playvideo_clicked.emit();  // does nothing
    openfile_clicked.emit();   // this openfile will be accepted
    playvideo_clicked.emit();
    volumechanged.emit(80);
    volumechanged.emit(110);   // reject this volume change
    volumechanged.emit(110);   // accept this one

    return 0;
}
