#pragma once

#include <functional>
#include <vector>

template <typename... SigArgs>
class signal
{
    using slot_t = std::function<void(SigArgs...)>;

public:
    void connect(slot_t slot)
    {
        _slots.push_back(slot);
    }

    void emit(SigArgs... args) const
    {
        for (const auto &slot : _slots)
            slot(args...);
    }

private:
    std::vector<slot_t> _slots;
};
