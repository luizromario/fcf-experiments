#include "signal.hpp"

#include <iostream>

using namespace std;

int main()
{
    signal<int> value_changed;

    value_changed.connect([](auto value) {
        cout << "changed to " << value << "\n";
    });

    value_changed.emit(10);
    value_changed.emit(20);

    value_changed.connect([&](auto value) {
        if (value > 100)
            value_changed.connect([] (auto) {
                cout << "  (value was changed to >100 in the past)\n";
            });
    });

    value_changed.emit(30);
    value_changed.emit(40);
    value_changed.emit(200);
    value_changed.emit(50);
    value_changed.emit(60);

    return 0;
}
