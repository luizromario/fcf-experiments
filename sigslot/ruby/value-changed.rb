require './mysignal'

value_changed = MySignal.new

value_changed.connect do |value|
    puts "changed to #{value}"
end

value_changed.emit 10
value_changed.emit 20

value_changed.connect do |value|
    if value > 100
        value_changed.connect do
            puts "  (value changed to >100 in the past)"
        end
    end
end

value_changed.emit 30
value_changed.emit 40
value_changed.emit 200
value_changed.emit 50
value_changed.emit 60
