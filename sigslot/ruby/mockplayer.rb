require './mysignal'

class Diag
    attr_reader :ok, :cancel

    def initialize
        @ok = MySignal.new
        @cancel = MySignal.new
    end
end

openfile_clicked = MySignal.new
filedialog = Diag.new
playvideo_clicked = MySignal.new
volumechanged = MySignal.new
highvolumedialog = Diag.new

openfile_clicked.connect do
    puts 'Open file (type in "cancel" to cancel)'

    res = gets.chomp
    if res == 'cancel'
        filedialog.cancel.emit
    else
        filedialog.ok.emit res
    end
end

filedialog.cancel.connect do
    puts 'User cancelled opening'
end

filedialog.ok.connect do |filename|
    puts "Opened #{filename}"

    playvideo_clicked.connect do
        puts "Playing #{filename}"

        volumechanged.connect do |volume|
            puts "Volume changed to #{volume}"

            if volume > 100
                puts 'Volume is too loud. Increase it anyway?'

                res = gets.chomp
                if res == 'yes'
                    highvolumedialog.ok.emit volume
                else
                    highvolumedialog.cancel.emit
                end
            end
        end
    end
end

highvolumedialog.cancel.connect do
    volumechanged.emit 100
end

openfile_clicked.emit   # this openfile will be cancelled
playvideo_clicked.emit  # does nothing
openfile_clicked.emit   # this openfile will be accepted
playvideo_clicked.emit
volumechanged.emit 80
volumechanged.emit 110  # reject this volume change
volumechanged.emit 110  # accept this one
