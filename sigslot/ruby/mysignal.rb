class MySignal  # can't use Signal for some reason
    def initialize
        @slots = []
    end

    def connect(&slot)  # slot.class == Proc
        @slots << slot
        nil
    end

    def emit(*args)
        @slots.each do |slot|
            slot.call(*args)
        end
        nil
    end
end
