function Dialog() {
    return {
        ok: new Signal(),
        cancel: new Signal()
    };
}

let openFileClicked = new Signal();
let fileDialog = new Dialog();
let playVideoClicked = new Signal();
let volumeChanged = new Signal();
let highVolumeDialog = new Dialog();

openFileClicked.connect(() => {
    let res = prompt("Open file (type in \"cancel\" to cancel)");

    if (res == "cancel") {
        fileDialog.cancel.emit();
    } else {
        fileDialog.ok.emit(res);
    }
});

fileDialog.cancel.connect(() => alert("User cancelled opening"));

fileDialog.ok.connect((filename) => {
    alert("Opened " + filename);
    playVideoClicked.connect(() => {
        alert("Playing " + filename); // somehow this alert is undefined

        volumeChanged.connect((volume) => {
            alert("Volume changed to " + volume);

            if (volume > 100) {
                if (confirm("Volume is too loud. Increase it anyway?")) {
                    highVolumeDialog.ok.emit(volume);
                } else {
                    highVolumeDialog.cancel.emit();
                }
            }
        });
    });
});

highVolumeDialog.cancel.connect(() => volumeChanged.emit(100));

openFileClicked.emit()  // this openfile will be cancelled
playVideoClicked.emit() // does nothing
openFileClicked.emit()  // this openfile will be accepted
playVideoClicked.emit()
volumeChanged.emit(80)
volumeChanged.emit(110) // reject this volume change
volumeChanged.emit(110) // accept this one
