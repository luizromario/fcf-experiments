function Signal() {
    return {
        slots: [],
        connect: function(slot) {
            this.slots.push(slot);
        },
        emit: function(...args) {
            this.slots.forEach((s) => s(...args));
        }
    }
}
