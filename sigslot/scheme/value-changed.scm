(load "signal.scm")

(define value-changed (new-signal))

(connect value-changed (lambda (value)
    (display "changed to ") (display value) (newline)
))

(emit value-changed 10)
(emit value-changed 20)

(connect value-changed (lambda (value)
    (if (> value 100)
        (connect value-changed (lambda (_)
            (display "  (value changed to >100 in the past")
            (newline)))
        '())
))

(emit value-changed 30)
(emit value-changed 40)
(emit value-changed 200)
(emit value-changed 50)
(emit value-changed 60)
