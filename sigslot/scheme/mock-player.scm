(load "signal.scm")

(define (new-diag) (cons (new-signal) (new-signal)))
(define (diag-ok diag) (car diag))
(define (diag-cancel diag) (cdr diag))

(let (
    (open-file-clicked (new-signal))
    (file-dialog (new-diag))
    (play-video-clicked (new-signal))
    (volume-changed (new-signal))
    (high-volume-dialog (new-diag)))

    (connect open-file-clicked (lambda ()
        (display "Open file (type in \"cancel\" to cancel)\n")
        (let (
            (res (read-line)))

            (if (equal? res "cancel")
                (emit (diag-cancel file-dialog))
                (emit (diag-ok file-dialog) res)))
    ))

    (connect (diag-cancel file-dialog) (lambda ()
        (display "User cancelled opening\n")))

    (connect (diag-ok file-dialog) (lambda (filename)
        (display "Opened ") (display filename) (newline)

        (connect play-video-clicked (lambda ()
            (display "Playing ") (display filename) (newline)

            (connect volume-changed (lambda (volume)
                (display "Volume changed to ") (display volume) (newline)

                (if (<= volume 100) '()
                    (begin
                        (display "Volume is too loud. Increase it anyway?\n")
                        (if (equal? (read-line) "yes")
                            (emit (diag-ok high-volume-dialog) volume)
                            (emit (diag-cancel high-volume-dialog)))))
            ))
        ))
    ))

    (connect (diag-cancel high-volume-dialog) (lambda ()
        (emit volume-changed 100)))

    (emit open-file-clicked)   ; this openfile will be cancelled
    (emit play-video-clicked)  ; does nothing
    (emit open-file-clicked)   ; this openfile will be accepted
    (emit play-video-clicked)
    (emit volume-changed 80)
    (emit volume-changed 110)  ; reject this volume change
    (emit volume-changed 110)  ; accept this one
)
