(define new-signal (lambda ()
    (define slots '())
    (define connect (lambda (slot)
        (set! slots (append slots (list slot)))
    ))
    (define emit (lambda args
        (define _emit (lambda (slots)
            (if (null? slots) '()
                (begin
                    (apply (car slots) args)
                    (_emit (cdr slots))))
        ))
        (_emit slots)
    ))

    (list connect emit)
))

(define connect (lambda (signal slot)
    ((car signal) slot)
))

(define emit (lambda (signal . args)
    (apply (cadr signal) args)
))
