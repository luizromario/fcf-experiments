class Diag<T> {
    let cancel = Signal<Void>()
    let ok = Signal<T>()
}

let openfile_clicked = Signal<Void>()
let file_dialog = Diag<String>()
let playvideo_clicked = Signal<Void>()
let volumechanged = Signal<Int>()
let highvolume_dialog = Diag<Int>()

openfile_clicked.connect {
    print("Open file (type in \"cancel\" to cancel)")

    let res = readLine()!
    if res == "cancel" {
        file_dialog.cancel.emit(())
    } else {
        file_dialog.ok.emit(res)
    }
}

file_dialog.cancel.connect { print("User cancelled opening") }

file_dialog.ok.connect { filename in
    print("Opened \(filename)")

    playvideo_clicked.connect {
        print("Playing \(filename)")

        volumechanged.connect { volume in
            if volume <= 100 {
                print("Volume changed to \(volume)")
            } else {
                print("Volume is too loud. Increase it anyway?")

                let res = readLine()!
                if res == "yes" {
                    highvolume_dialog.ok.emit(volume)
                } else {
                    highvolume_dialog.cancel.emit(())
                }
            }
        }
    }
}

highvolume_dialog.cancel.connect { volumechanged.emit(100) }
highvolume_dialog.ok.connect { volume in
    print("Volume changed to \(volume)")
}

openfile_clicked.emit(())   // this openfile will be cancelled
playvideo_clicked.emit(())  // does nothing
openfile_clicked.emit(())   // this openfile will be accepted
playvideo_clicked.emit(())
volumechanged.emit(80)
volumechanged.emit(110)     // reject this volume change
volumechanged.emit(110)     // accept this one
