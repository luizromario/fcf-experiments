var valueChanged = Signal<Int>()

valueChanged.connect({ value in print("changed to \(value)") })

valueChanged.emit(10)
valueChanged.emit(20)

// trailing closure provides a similar syntax sugar as Ruby's blocks
valueChanged.connect { value in
    if value > 100 {
        valueChanged.connect({ _ in
            print("  (value changed to >100 in the past)")
        })
    }
}

valueChanged.emit(30)
valueChanged.emit(40)
valueChanged.emit(200)
valueChanged.emit(50)
valueChanged.emit(60) 
