public class Signal<T> {
    var slots: [(T) -> ()] = []
    func connect(_ slot: @escaping (T) -> ()) {
        slots.append(slot)
    }

    func emit(_ arg: T) {
        for slot in slots {
            slot(arg)
        }
    }
}
