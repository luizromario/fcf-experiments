from signal import *

def mk_emitter():
    count = 0
    s = Signal()

    def on_s_emitted():
        nonlocal count
        count = count + 1
        if count > 5:
            print("emitted more than five times")
    s.connect(on_s_emitted)

    return lambda: s.emit()

emit1 = mk_emitter()
emit2 = mk_emitter()

emit1()
emit1()
emit1()
emit1()
emit1()
emit1()
