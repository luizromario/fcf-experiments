from signal import Signal

value_changed = Signal()

value_changed.connect(
    lambda value:
        print("changed to %s" % value))

value_changed.emit(10)
value_changed.emit(20)

# Python will not support more complex slots like other languages do
value_changed.connect(
    lambda value:
        value_changed.connect(
            lambda _:
                print("  (value changed to >100 in the past)"))
        if value > 100 else None)

value_changed.emit(30)
value_changed.emit(40)
value_changed.emit(200)
value_changed.emit(50)
value_changed.emit(60)
