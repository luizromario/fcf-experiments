from signal import Signal

class Diag:
    def __init__(self):
        self.cancel = Signal()
        self.ok = Signal()

openfile_clicked = Signal()
file_dialog = Diag()
playvideo_clicked = Signal()
volumechanged = Signal()
highvolume_dialog = Diag()

# Python lambdas only support single expressions and don't support statements,
# so sometimes it just isn't practicable to use a lambda

# There are some tricks to allow multiple expressions inside a lambda, but these
# are usually unreliable and lead to very unreadable code (example:
# https://stackoverflow.com/a/17604249)
def on_openfile_clicked():
    res = input("Open file (type in \"cancel\" to cancel)\n")

    if res == "cancel":
        file_dialog.cancel.emit()
    else:
        file_dialog.ok.emit(res)
openfile_clicked.connect(on_openfile_clicked)

file_dialog.cancel.connect(lambda: print("User cancelled opening"))

# ...but, since Python allows nested functions, has proper lexical scoping, and
# functions are first-class values (even def'd functions), having limited
# lambdas is not a showstopper -- although it can be a bit tedious and
# error-prone
def on_file_dialog_ok(filename):
    print("Opened " + filename)

    def on_playvideo_clicked():
        print("Playing " + filename)

        def on_volumechanged(volume):
            if volume <= 100:
                print("Volume changed to " + str(volume))
            else:
                res = input("Volume is too loud. Increase it anyway?\n")

                if res == "yes":
                    highvolume_dialog.ok.emit(volume)
                else:
                    highvolume_dialog.cancel.emit()
        volumechanged.connect(on_volumechanged)
    playvideo_clicked.connect(on_playvideo_clicked)
file_dialog.ok.connect(on_file_dialog_ok)

highvolume_dialog.cancel.connect(lambda: volumechanged.emit(100))
highvolume_dialog.ok.connect(lambda volume:
    print("Volume changed to " + str(volume)))

openfile_clicked.emit()   # this openfile will be cancelled
playvideo_clicked.emit()  # does nothing
openfile_clicked.emit()   # this openfile will be accepted
playvideo_clicked.emit()
volumechanged.emit(80)
volumechanged.emit(110)   # reject this volume change
volumechanged.emit(110)   # accept this one
