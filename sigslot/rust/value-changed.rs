mod signal;

use std::rc::Rc;
use signal::Signal;

fn main() {
    let value_changed = Rc::new(Signal::new());

    value_changed.connect(|value| {
        println!("changed to {}", value)
    });

    value_changed.emit(10);
    value_changed.emit(20);

    value_changed.connect({
        // will necessarily need to explictily clone the Rc pointer, because
        // cloning is always explicit
        
        // in fact, this is a pretty common idiom (see
        // https://github.com/rust-lang/rfcs/issues/2407)
        let value_changed = value_changed.clone();
        move |&value| {
            if value > 100 {
                value_changed.connect(|_| {
                    println!("  (value changed to >100 in the past)")
                });
            }
        }
    });

    value_changed.emit(30);
    value_changed.emit(40);
    value_changed.emit(200);
    value_changed.emit(50);
    value_changed.emit(60);
    // although both the closure above and value_changed die here, rust has
    // no concept of two variables falling out of scope at the same time
    
    // variables go out of scope in the reverse order of their creation,
    // so value_changed will die first, which is why you can't just borrow
    // it inside of the closure
}
