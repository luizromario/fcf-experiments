use std::cell::RefCell;

pub struct Signal<'a, T> {
    slots: RefCell<Vec<Box<FnMut(&T) + 'a>>>,
    deferred: RefCell<Vec<Box<FnMut(&T) + 'a>>>
}

impl<'a, T> Signal<'a, T> {
    pub fn new() -> Signal<'a, T> {
        Signal {
            slots: RefCell::new(Vec::new()),
            deferred: RefCell::new(Vec::new())
        }
    }

    pub fn connect<SlotT: 'a>(self: &Self, slot: SlotT) where SlotT: FnMut(&T) {
        match self.slots.try_borrow_mut() {
            Ok(slots) => slots,
            Err(_) => self.deferred.borrow_mut()
        }.push(Box::new(slot))
    }

    pub fn emit(self: &Self, arg: T) {
        for slot in self.slots.borrow_mut().iter_mut() {
            slot(&arg)
        }

        let deferred = RefCell::new(Vec::new());
        self.deferred.swap(&deferred);

        for slot in deferred.into_inner().into_iter() {
            self.slots.borrow_mut().push(slot)
        }
    }
}
