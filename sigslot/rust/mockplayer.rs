mod signal;

use std::io::{self, Read};
use std::rc::Rc;
use signal::Signal;

struct Dialog<'a, T> {
    cancel: Signal<'a, ()>,
    ok: Signal<'a, T>
}

impl<'a, T> Dialog<'a, T> {
    fn new() -> Dialog<'a, T> {
        Dialog {
            cancel: Signal::new(),
            ok: Signal::new()
        }
    }
}

// WIP

fn main() {
    let openfile_clicked = Rc::new(Signal::new());
    let file_dialog = Rc::new(Dialog::new());
    let playvideo_clicked = Rc::new(Signal::new());
    let volumechanged = Rc::new(Signal::new());
    let highvolume_dialog = Rc::new(Dialog::new());
    
    openfile_clicked.connect(|_| {
        println!("Open file (type in \"cancel\" to cancel)");
        
        let mut res;
        io::stdin().lock().read_to_string(&mut res);
        
        if res == "cancel" {
            file_dialog.cancel.emit(())
        } else {
            file_dialog.ok.emit(res);
        }
    });
    
    file_dialog.cancel.connect(|_| println!("User cancelled opening"));
    
    file_dialog.ok.connect(|&filename| {
        println!("Opened {}", filename);
    
        playvideo_clicked.connect(|_| {
            println!("Playing {}", filename);
            
            volumechanged.connect(|&volume| {
                println!("Volume changed to {}", volume);
                
                if volume > 100 {
                    println!("Volume is too loud. Increase it anyway?");
                    
                    let mut res;
                    io::stdin().lock().read_to_string(&mut res);
                    
                    if res == "yes" {
                        highvolume_dialog.ok.emit(volume)
                    } else {
                        highvolume_dialog.cancel.emit(())
                    }
                }
            })
        })
    });
    
    highvolume_dialog.cancel.connect(|_| volumechanged.emit(100));
    
    openfile_clicked.emit(());   // this openfile will be cancelled
    playvideo_clicked.emit(());  // does nothing
    openfile_clicked.emit(());   // this openfile will be accepted
    playvideo_clicked.emit(());
    volumechanged.emit(80);
    volumechanged.emit(110);     // reject this volume change
    volumechanged.emit(110);     // accept this one
}
