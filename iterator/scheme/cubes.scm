(load "iterate.scm")

(display "These are the first 10 cubes larger than 12345: ")
(let ((big-cubes (it-filter
                  (it-map (mk-count) (lambda (n) (* n n n)))
                  (lambda (n) (> n 12345)))))
  (print-it (get-n big-cubes 10))
)
