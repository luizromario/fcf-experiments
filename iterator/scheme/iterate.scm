(define (each-of-it it f)
  (let ((res (it)))
    (if (not (null? res))
        (begin (f res)
               (each-of-it it f)))))

(define (print-els it)
  (each-of-it it (lambda (el) (display el)
                              (display " ")))
  (newline))

(define-syntax iterator
  (syntax-rules (with)
    ((iterator with bindings body ...)
     (let bindings (lambda () body ...)))
    ((iterator body ...)
     (lambda () body ...))))

(define-syntax define-iterator
  (syntax-rules ()
    ((define-iterator name body ...)
     (define name (iterator body ...)))))

(define (it-count)
  (let ((c 0)) (lambda ()
                 (set! c (+ c 1))
                 c)))

(define (it-get-n it n)
  (let ((c 0)) (lambda ()
                 (if (eq? c n) '()
                     (begin (set! c (+ c 1))
                            (it))))))

(define (container->it stop-cond get-cur shift-el)
  (lambda ()
    (if (stop-cond) '()
        (let ((cur (get-cur)))
          (shift-el)
          cur))))
  
(define (list->it l)
  (container->it (lambda () (null? l))
                 (lambda () (car l))
                 (lambda () (set! l (cdr l)))))

(define (vector->it v)
  (let ((i 0))
    (container->it (lambda () (<= (vector-length v) i))
                   (lambda () (vector-ref v i))
                   (lambda () (set! i (+ i 1))))))

(define (it->list it)
  (let ((res (it))) (if (null? res) '()
                        (cons res (it->list it)))))

(define (it-cycle-list l)
  (let ((v (list->vector l))
        (i 0))
    (lambda () (let ((el (vector-ref v i)))
                 (set! i (+ i 1))
                 (if (eq? (vector-length v) i) (set! i 0))
                 el))))    

(define (it-map it f)
  (lambda () (let ((res (it)))
               (if (null? res) '()
                   (f res)))))

(define (it-filter it c)
  (define (new-it) (let ((res (it)))
                     (cond ((null? res) '())
                           ((not (c res)) (new-it))
                           (else res))))
  new-it)

(define (it-append it1 it2)
  (lambda () (let ((res1 (it1)))
               (if (null? res1) (it2) res1))))