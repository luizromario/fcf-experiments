#ifndef ITERATOR_H
#define ITERATOR_H

struct _it_response
{
    int null;
    double val;
};
typedef struct _it_response it_response;

typedef it_response(*iterator_f)(void *);
struct _iterator_c
{
    void *env;
    iterator_f f;
};
typedef struct _iterator_c iterator_c;

it_response exec_it(iterator_c it);

void each_it(iterator_c it, void(*f)(double));
void print_it(iterator_c it);

void destruct_it(iterator_c it);

iterator_c it_count(void);
iterator_c it_get_n(iterator_c it, int n);
iterator_c it_map(iterator_c it, double(*f)(double));
iterator_c it_filter(iterator_c it, int(*c)(double));

#endif
