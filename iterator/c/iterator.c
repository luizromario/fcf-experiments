#include "iterator.h"

#include <stdio.h>
#include <stdlib.h>

it_response exec_it(iterator_c it)
{
    return it.f(it.env);
}

void each_it(iterator_c it, void(*f)(double))
{
    for (;;) {
        it_response res = exec_it(it);
        if (res.null)
            return;
        
        f(res.val);
    }
}

static void print_val(double v)
{
    printf("%lf ", v);
}

void print_it(iterator_c it)
{
    each_it(it, print_val);
    printf("\n");
}

void destruct_it(iterator_c it)
{
    free(it.env);
}

typedef struct _count_e
{
    int count;
} count_e;

static it_response count_f(void *env)
{
    count_e *e = (count_e *) env;
    it_response res;

    e->count++;
    res.null = 0;
    res.val = e->count;
    return res;
}

iterator_c it_count(void)
{
    iterator_c it;
    it.env = malloc(sizeof(count_e));
    it.f = count_f;
    
    return it;
}

typedef struct _get_n_e
{
    iterator_c it;
    int n;
    int c;
} get_n_e;

static it_response get_n_f(void *env)
{
    get_n_e *e = (get_n_e *) env;
    if (e->c == e->n) {
        it_response res;
        res.null = 1;
        return res;
    }

    e->c++;
    return exec_it(e->it);
}

iterator_c it_get_n(iterator_c it, int n)
{
    iterator_c get_n;
    get_n_e *e = (get_n_e *) malloc(sizeof(get_n_e));
    
    e->it = it;
    e->n = n;
    e->c = 0;

    get_n.env = e;
    get_n.f = get_n_f;

    return get_n;
}

typedef struct _map_e
{
    iterator_c it;
    double(*f)(double);
} map_e;

static it_response map_f(void *env)
{
    map_e *e = (map_e *) env;
    it_response res = exec_it(e->it);
    if (res.null) {
        it_response res;
        res.null = 1;
        return res;
    }

    res.val = e->f(res.val);
    return res;
}

iterator_c it_map(iterator_c it, double(*f)(double))
{
    iterator_c map;
    map_e *e = (map_e *) malloc(sizeof(map_e));

    e->it = it;
    e->f = f;

    map.env = e;
    map.f = map_f;

    return map;
}  

typedef struct _filter_e
{
    iterator_c it;
    int(*c)(double);
} filter_e;

static it_response filter_f(void *env)
{
    filter_e *e = (filter_e *) env;
    it_response res = exec_it(e->it);
    if (res.null) {
        it_response res;
        res.null = 1;
        return res;
    } else if (!e->c(res.val)) {
        return filter_f(env);
    } else {
        return res;
    }
}

iterator_c it_filter(iterator_c it, int(*c)(double))
{
    iterator_c filter;
    filter_e *e = (filter_e *) malloc(sizeof(map_e));

    e->it = it;
    e->c = c;

    filter.env = e;
    filter.f = filter_f;

    return filter;
}

static double cube(double n) { return n * n * n; }
static int large(double n) { return n > 150000; }

int main()
{
    print_it(it_get_n(it_filter(it_map(it_count(), cube), large), 10));
}
