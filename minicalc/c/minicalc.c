#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef double (*op_t)(double);

double id(double n) { return n; }

double value = 0;
op_t op = id;

double sum(double n) { return value + n; }
double mult(double n) { return value * n; }

void parse_arg(const char *arg) {
    if (strcmp(arg, "sum") == 0)
        op = sum;
    else if (strcmp(arg, "mult") == 0)
        op = mult;
    else
        value = op(atof(arg));
}

int main(int argc, char *argv[]) {
    for (int i = 1; i < argc; ++i)
        parse_arg(argv[i]);
    
    printf("%lf\n", value);
}
