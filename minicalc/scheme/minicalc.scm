(let ((value 0)
      (op (lambda (n) n)))
  (define (sum n) (+ value n))
  (define (mult n) (* value n))

  (define (change-op name)
    (cond ((equal? name "sum") (set! op sum))
          ((equal? name "mult") (set! op mult))))
  
  (define (parse-arg a)
    (let ((n (string->number a)))
      (if (number? n)
          (set! value (op n))
          (change-op a))))

  (for-each parse-arg (command-line-arguments))
  (display value) (newline)
)
