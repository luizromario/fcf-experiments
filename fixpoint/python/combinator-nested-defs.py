def combinator(stepper):
    def make_step(next_step):
        def step(arg):
            return stepper(next_step(next_step))(arg)
        
        return step
    
    return make_step(make_step)

def fact_step(next):
    def step(n):
        if n == 0:
            return 1
        
        return n * next(n - 1)
    
    return step

fact = combinator(fact_step)

print(fact(10))
