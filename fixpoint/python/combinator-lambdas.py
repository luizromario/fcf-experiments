def combinator(stepper):
    def make_step(next_step):
        return (lambda arg:
            stepper(next_step(next_step))(arg))
    
    return make_step(make_step)

def fact_step(next):
    return (lambda n:
        1 if n == 0 else n * next(n - 1))

fact = combinator(fact_step)

print(fact(10))
