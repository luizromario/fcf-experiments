function combinator(stepper)
    local function make_step(next_step)
        return function(arg)
            return stepper(next_step(next_step))(arg)
        end
    end
    return make_step(make_step)
end

function fact_step(next)
    return function(n)
        if n == 0 then return 1 end

        return n * next(n - 1)
    end
end