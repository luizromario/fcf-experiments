#include <iostream>

template <typename S>
auto combinator(S stepper)
{
    auto make_step = [=](auto next_step)
    {
        return [=](auto arg)
        {
            return stepper(next_step(next_step))(arg);
        };
    };
    
    return make_step(make_step);
}

template <typename N>
auto fact_step(N next)
{
    return [=](auto n)
    {
        return n == 0? 1 : n * next(n - 1);
    };
}

int main()
{
    // wrong: fact_step has to be a concrete function
    auto fact = combinator(fact_step);
    
    std::cout << fact(10) << "\n";
    
    return 0;
}
