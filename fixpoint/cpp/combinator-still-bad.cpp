// WIP

#include <functional>
#include <iostream>
#include <cstdio>

template <typename F>
class y_result
{
    F f;
public:
    y_result(F f) : f(f) {}

    template <typename Arg>
    auto operator()(Arg arg) -> decltype(f(*this, arg))
    {
	return f(*this, arg);
    }
};

template <typename F>
y_result<F> Y(F f)
{
    return {f};
}

int main()
{
    auto fact = Y([](auto self, int n)
		  {
		      return n == 0? 1 : n * self(n - 1);
		  });
		  		      
    printf("%d\n", fact(10));
    
    return 0;
}
