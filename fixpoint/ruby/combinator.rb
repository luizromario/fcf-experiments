def combinator
    make_step = lambda do |next_step|
        lambda do |arg|
            (yield next_step.call(next_step)).call(arg)
        end
    end

    make_step.call make_step
end

fact = combinator do |next_|
    lambda do |n|
        n == 0? 1 : n * next_.call(n - 1)
    end
end

puts fact.call(10)
