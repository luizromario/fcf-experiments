#include <iostream>

class member
{
public:
    member() = default;
    member(const member &other)
    {
        value = other.value;
        std::cout << "member copied\n";
    }
    member(member &&other)
    {
        value = other.value;
        std::cout << "member moved\n";
    }

    ~member()
    {
        std::cout << "member deleted\n";
    }

    int value = 0;
};

class thing
{
public:
    thing() = default;
    thing(thing &&) = delete;
    thing(const thing &) = delete;

    ~thing()
    {
        std::cout << "thing _was_, in fact, deleted (for good)\n";
    }

    auto complex_calculations(int other_value)
    {
        // we can now set a custom initializer for m, so that we can
        // copy it directly into the lambda. Also, we no longer need
        // std::function, so only one copy is needed:

        // [Output]
        // 10
        // member copied
        // thing _was_, in fact, deleted (for good)
        // member deleted
        // 60
        // member deleted

        // however, something strange happens if we remove the custom
        // initializer
        // return [=](int yet_another_value)

        // [Output]
        // 10
        // thing _was_, in fact, deleted (for good)
        // member deleted
        // 60

        // it works as it should, even though m was deleted before
        // the lambda was called

        return [=, m(m)](int yet_another_value)
        {
            return m.value + other_value + yet_another_value;
        };
    }

    void set(int value)
    {
        m.value = value;
    }

private:
    member m;
};

auto make_complex_calculations(int value, int other_value)
{
    thing t;
    t.set(value);

    std::cout << value << "\n";

    return t.complex_calculations(other_value);
}

int main()
{
    auto cc = make_complex_calculations(10, 20);
    std::cout << cc(30) << "\n";
}
