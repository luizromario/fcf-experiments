#include <iostream>
#include <functional>

class member
{
public:
    member() = default;
    member(const member &other)
    {
        value = other.value;
        std::cout << "member copied\n";
    }
    member(member &&other)
    {
        value = other.value;
        std::cout << "member moved\n";
    }

    ~member()
    {
        std::cout << "member deleted\n";
    }

    int value = 0;
};

class thing
{
public:
    thing() = default;
    thing(thing &&) = delete;
    thing(const thing &) = delete;

    ~thing()
    {
        std::cout << "thing _was_, in fact, deleted (for good)\n";
    }

    std::function<int(int)> complex_calculations(int other_value)
    {
        // unfortunately, inferring the return type of functions (i.e. auto f();)
        // isn't available in C++11, so we have to wrap the lambda in an
        // std::function, and it shows its overhead:

        // [Output]
        // 10
        // member copied
        // member moved    <--- here, m was likely moved into std::function
        //                      upon return
        // member deleted
        // thing _was_, in fact, deleted (for good)
        // member deleted
        // 60
        // member deleted

        // also, there are no custom initializers inside the capture block
        // in C++11 either. Automatic copy-capturing won't work because
        // the lambda will, instead, capture this, and m will *not* be
        // copied:

        // [Output]
        // 10
        // thing _was_, in fact, deleted (for good)
        // member deleted
        // 32814
        auto &m = this->m;
        return [=](int yet_another_value)
        {
            return m.value + other_value + yet_another_value;
        };
    }

    void set(int value)
    {
        m.value = value;
    }

private:
    member m;
};

std::function<int(int)> make_complex_calculations(int value, int other_value)
{
    thing t;
    t.set(value);

    std::cout << value << "\n";

    return t.complex_calculations(other_value);
}

int main()
{
    auto cc = make_complex_calculations(10, 20);
    std::cout << cc(30) << "\n";
}
