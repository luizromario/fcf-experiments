#include <iostream>

class A
{
public:
    int hello(int n) {
        return n + y;
    }

    int y;
};

typedef int (A::*pointer_to_member)(int);
typedef int (*pointer_to_function)(A*, int);

int hi(A hey, pointer_to_function sup) {
    return sup(&hey, 10);
}

int main() {
//     // cannot convert
//     std::cout << hi(A{20}, static_cast<pointer_to_function>(&A::hello)) << "\n";

    // works on g++; doesn't work on clang++
    std::cout << hi(A{20}, reinterpret_cast<pointer_to_function>(&A::hello)) << "\n";
}