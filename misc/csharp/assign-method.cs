using System;

delegate void IntSetter(int val);

class Program
{
    class Hello
    {
        int a = 0;

        public void SetA(int a)
        {
            this.a = a;
        }

        public int GetASqr()
        {
            return a * a;
        }
    }

    public static void Main()
    {
        var h = new Hello();
        // error CS0815: An implicitly typed local variable declaration cannot be initialized with `method group'
        // error CS0119: Expression denotes a `variable', where a `method group' was expected
        // var setA = h.SetA;
        IntSetter setA = h.SetA;
        setA(10);
        Console.WriteLine(h.GetASqr());
    }
}
