class Mutable<T>{
    private T value;

    public Mutable(T v) { set(v); }
    public T get() { return value; }
    public void set(T v) { value = v; }
}

class ModifyString {
    interface Runnable {
        void run();
    }

    public static void main(String[] args) {
        Mutable<String> hello = new Mutable<String>("Hello");

        Runnable modifyHello = () -> hello.set("Hi");
        modifyHello.run();

        System.out.println(hello.get());
    }
}