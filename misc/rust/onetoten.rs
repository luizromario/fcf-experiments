fn main() {
    let mut funcs = Vec::new();

    for i in 0..10 {
        // not explicitly marking the closure as move will make the
        // compiler infer a borrow, making the compilation obviously
        // fail
        funcs.push(move || i);
    }

    for f in funcs.into_iter() {
        println!("{}", f());
    }
}
