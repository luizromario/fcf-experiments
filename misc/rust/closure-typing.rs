fn main() {
    let mut fnvec = Vec::new();

// error[E0308]: mismatched types
//  --> closure-typing.rs:5:16
//   |
// 5 |     fnvec.push(|x| x);
//   |                ^^^^^ expected closure, found a different closure
//   |
//   = note: expected type `[closure@closure-typing.rs:4:16: 4:21]`
//              found type `[closure@closure-typing.rs:5:16: 5:21]`
//   = note: no two closures, even if identical, have the same type
//   = help: consider boxing your closure and/or using it as a trait object

//     // Two different closure values with two different types
//     fnvec.push(|x| x);
//     fnvec.push(|x| x);

    // Two different closure values that share the same type because
    // they were instantiated from the same closure expression
    for _ in 0..2 {
        fnvec.push(|x| x)
    }

    for f in fnvec.into_iter() {
        println!("{}", f(10))
    }
}