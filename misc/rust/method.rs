struct Test {
    i: u64
}

impl Test {
    fn f(&self) -> u64 { self.i }
}

fn main() {
    let t = Test { i: 0 };
    // cannot access method like field (E0615)
    let m = t.f;
    println!("{}", m());
}