# nested method definitions do not access their outer environment
class Nested
    def a(x)
        def b(y)
            x + y  # undefined local variable or method `x'
        end

        b(10)
    end
end

# In Test, a new definition is generated dynamically when a is called.
# That is, before a is called, there will be no b method available for
# the Test class; after that, the b method will be available for all
# instances of Test, since def modifies the class, not the instance
class Test
    def a(x)
        def b(y)
            10 + y
        end

        b(12)
    end
end

class Test2
    def a(x)
        b(12)
    end

    def b(y)
        10 + y
    end
end
