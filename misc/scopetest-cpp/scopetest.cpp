#include <iostream>

using namespace std;

class scope_logger
{
    static int id;
    int cur_id;
    int cp_id = 0;

    public:
        scope_logger():cur_id(id++) { cout << "defctor for " << cur_id << "\n"; }
        scope_logger(const scope_logger &other):cur_id(other.cur_id), cp_id(other.cp_id + 1) { cout << "cpctor for " << cur_id << " from cp_id " << other.cp_id << "\n"; }
        scope_logger(scope_logger &&other):cur_id(other.cur_id) { cout << "mvctor for " << cur_id << "\n"; }

        scope_logger &operator=(const scope_logger &other)
        {
            cur_id = other.cur_id;
            cout << "cpasgn for " << cur_id << "\n";
        }

        scope_logger &operator=(scope_logger &&other)
        {
            cur_id = other.cur_id;
            cout << "mvasgn for " << cur_id << "\n";
        }

        ~scope_logger() { cout << "dtor for " << cur_id << " cp_id " << cp_id << "\n"; }
};

int scope_logger::id = 0;

int main()
{
    scope_logger a, b, c;

    auto l = [=]{a; return [&]{b;};}();

    l();
}

