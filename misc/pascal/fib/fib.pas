program Fib;

function Fib(n: integer): integer;
var
    i: integer;
    Prev1: integer;
    Prev2: integer;
begin
    if n <= 1 then
        Fib := n
    else
    begin
        Prev1 := 0;
        Prev2 := 1;

        for i := 2 to n do
        begin
            Fib := Prev1 + Prev2;
            Prev1 := Prev2;
            Prev2 := Fib;
        end
    end
end;

var
    N: integer;
    Code: integer;

begin
    if ParamCount <> 1 then
        writeln('usage: ', ParamStr(0), ' <n>')
    else
    begin
        Val(ParamStr(1), N, Code);
        writeln('fib ', ParamStr(1),': ', Fib(N));
    end
end.
