{$modeswitch nestedprocvars}
program Pars;

type
    ToInteger = function(): integer;
    ToIntegerNested = function(): integer is nested;

var
    Thing3: ToIntegerNested;

function Thing: integer;
begin
    Thing := 10;
end;

procedure call(F: ToIntegerNested);
begin
    writeln(F);
end;

procedure Weird;
var
    Bla: integer;
    function Thing2: integer;
    begin
        Thing2 := Bla;
    end;
begin
    Bla := 15;
    Thing3 := @Thing2;
    call(Thing3);
end;

begin
    Weird;
    call(Thing3);
end.
