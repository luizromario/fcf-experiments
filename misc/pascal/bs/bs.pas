program bs;

Type
    IntArray = Array[1..20] of integer;

function Search(arr: IntArray; n: integer): integer;
    procedure SearchRange(b: integer; e: integer);
    var
        Mid: integer;
    begin
        Mid := (b + e) div 2;
        if b > e then Search := 0
        else if n = arr[Mid] then Search := Mid
        else if n > arr[Mid] then SearchRange(Mid + 1, e)
        else SearchRange(b, Mid - 1);
    end;
begin
    SearchRange(1, 20);
end;

const
    MyArr: IntArray = (
        1, 2, 5, 7, 12, 18, 19, 20, 30, 31,
        33, 38, 40, 41, 50, 60, 66, 67, 68, 80
    );

var
    Value: integer;
    Code: integer;

begin
    Val(ParamStr(1), Value, Code);
    WriteLn('Index for ', Value, ': ', Search(MyArr, Value));
end.
