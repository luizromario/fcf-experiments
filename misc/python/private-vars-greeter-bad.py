# bad example, only works if you explicitly the variables being used inside
# of the functions as nonlocal
class Greeter:
    def __init__(self):
        greeting = "Hello, "
        name = "stranger"

        def morning():
            greeting = "Good morning, "

        def evening():
            greeting = "Good evening, "

        def set_name(new_name):
            name = new_name

        def greet():
            print(greeting + name)

        self.morning = morning
        self.evening = evening
        self.set_name = set_name
        self.greet = greet

g = Greeter()
g.greet()
g.morning()  # this will do nothing
g.greet()
