class Me:
    def __init__(self):
        phrase = "i am angry"

        def yell():
            print(phrase)

        def angrier():
            phrase = "I AM ANGRY"

        self.yell = yell
        self.angrier = angrier

me = Me()
me.yell()
me.angrier()
me.yell()
