#include <stdio.h>

void (*P)();

void f(int a)
{
    void p() { printf("a = %d\n", a); }

    if (a == 1) {
        P = p;
        f(2);
        P = NULL;
    } else if (a == 2) {
        P();
    } else  {
        printf("asifude\n");
    }
}

void main(int c, char **a)
{
    f(1);
}

