#include <stdio.h>

typedef int (*int_to_int)(int);

int_to_int plus(int x) {
    int plusx(int y) {
        return x + y;
    }

    return plusx;
}

typedef int (*void_to_int)(void);

void_to_int mk_counter(void) {
    int n = 0;
    int counter(void) {
        return n++;
    }

    return counter;
}

int main(void) {
    // if you uncomment this, the counter code below will always work
    // if you comment this, it may throw a SIGSEGV or a SIGILL
    // or it could just run normally, the behavior is completely unpredictable
    //printf("2 + 2 is %d\n", plus(2)(2));

    void_to_int counter = mk_counter();

    for (int i = 0; i < 10; ++i)
        printf("%d ", counter());
    printf("\n");

    return 0;
}
