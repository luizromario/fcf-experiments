(define (point x y) (cons x y))
(define (x-coord p) (car p))
(define (y-coord p) (cdr p))

(define (sqr n) (* n n))

(define (circle r c)
  (lambda (p)
    (< (+ (sqr (- (x-coord p) (x-coord c)))
          (sqr (- (y-coord p) (y-coord c))))
       (sqr r))))

(define (rectangle tl br)
  (lambda (p)
    (and (and (> (x-coord p) (x-coord tl))
              (< (x-coord p) (x-coord br)))
         (and (> (y-coord p) (y-coord tl))
              (< (y-coord p) (y-coord br))))))

(define (outside region)
  (lambda (p) (not (region p))))

(define (intersect region1 region2)
  (lambda (p) (and (region1 p) (region2 p))))

(define (annulus r1 r2 c)
  (intersect (outside (circle r1 c)) (circle r2 c)))

(define (for-range from to body)
  (define (loop index)
    (if (< index (+ to 1))
        (begin (body index)
               (loop (+ index 1)))))
  (loop from))

(define (plot-ascii region)
  (for-range -10 10
             (lambda (y)
               (for-range -10 10
                          (lambda (x)
                            (if (region (point x y))
                                (display "#")
                                (display "."))))
               (newline))))

(define (plot-pgm region)
  (define image (open-output-file "image.pgm"))
  (display "P2 512 512 1" image)
  (newline image)
  (for-range -256 255
             (lambda (y)
               (for-range -256 255
                          (lambda (x)
                            (if (region (point x y))
                                (display "0 " image)
                                (display "1 " image))))))
  (close-output-port image))