(define small 0.0000001)

(define (fixed-point f first-guess)
    (define tolerance small)
    (define (close-enough? v1 v2)
        (< (abs (- v1 v2)) tolerance))
    (define (try guess)
        (let ((next (f guess)))
            (if (close-enough? guess next) next (try next))))

    (try first-guess))

; g'(x) = (g(x + dx) - g(x)) / dx
(define (deriv g)
    (define dx small)
    (lambda (x)
        (/ (- (g (+ x dx)) (g x))
            dx)))

(define (newton-transform g)
    (lambda (x)
        (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
    (fixed-point (newton-transform g) guess))

(define (square-root x)
    (newtons-method (lambda (y) (- (square y) x)) 1))
