SMALL = 0.0000001

def is_close_enough(v1, v2):
    return abs(v1 - v2) < SMALL

def fixed_point(f, first_guess):
    def try_fp(guess):
        next_guess = f(guess)
        if is_close_enough(guess, next_guess):
            return next_guess

        return try_fp(next_guess)

    return try_fp(first_guess)

def deriv(g):
    dx = SMALL
    return lambda x: (g(x + dx) - g(x)) / dx

def newton_transform(g):
    return lambda x: x - g(x) / deriv(g)(x)

def newtons_method(g, guess):
    return fixed_point(newton_transform(g), guess)

def square_root(x):
    return newtons_method(lambda y: y * y - x, 1)
