const small = 0.0000001

function isCloseEnough(v1, v2) {
    return Math.abs(v1 - v2) < small;
}

function fixedPoint(f, firstGuess) {
    function tryFp(guess) {
        let next = f(guess);
        if (isCloseEnough(guess, next)) {
            return next;
        } else {
            return tryFp(next);
        }
    }

    return tryFp(firstGuess);
}

function deriv(g) {
    const dx = small;
    return (x) => (g(x + dx) - g(x)) / dx;
}

function newtonTransform(g) {
    return (x) => x - g(x) / deriv(g)(x);
}

function newtonsMethod(g, guess) {
    return fixedPoint(newtonTransform(g), guess);
}

function squareRoot(x) {
    return newtonsMethod((y) => y * y - x, 1);
}

console.log(Math.sqrt(10));
console.log(squareRoot(10));
