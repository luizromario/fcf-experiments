small = 0.0000001

local function is_close_enough(v1, v2)
    return math.abs(v1 - v2) < small
end

function fixed_point(f, first_guess)
    local function try(guess)
        local next = f(guess)
        if is_close_enough(guess, next) then
            return next
        else
            return try(next)
        end
    end

    return try(first_guess)
end

function deriv(g)
    local dx = small
    return function(x)
        return (g(x + dx) - g(x)) / dx
    end
end

function newton_transform(g)
    return function(x)
        return x - g(x) / deriv(g)(x)
    end
end

function newtons_method(g, guess)
    return fixed_point(newton_transform(g), guess)
end

function square_root(x)
    return newtons_method(function(y)
        return y * y - x
    end, 1)
end
