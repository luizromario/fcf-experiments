const SMALL: f64 = 0.0000001;

trait Newton {
    fn fixed_point(self: &Self, first_guess: f64) -> f64;
    fn as_deriv(self: &Self) -> Box<Fn(f64) -> f64>;
}

fn is_close_enough(v1: f64, v2: f64) -> bool {
    (v1 - v2).abs() < SMALL
}

// WIP
// trying to make all newton related functions related to a trait
// in order to solve lifetime problems -- if I attach the lifetime
// of the returned functions to the lifetime of the self, maybe we
// won't have lifetime problems?
//
// a further complication is that I only have Rust 1.21 installed,
// which doesn't have impl Trait available. That makes returning
// lambdas a pain
impl<Function> Newton for Function
    where Function: Fn(f64) -> f64
{
    fn fixed_point(self: &Self, first_guess: f64) -> f64 {
        let mut guess = self(first_guess);

        loop {
            let next = self(guess);
            if is_close_enough(guess, next) {
                return next;
            }
            guess = next;
        }
    }

    fn as_deriv(self: &Self) -> Box<Fn(f64) -> f64> {
        let dx = SMALL;
        Box::new(|x| (self(x + dx) - self(x)) / dx)
    }
}

fn newton_transform<Function>(g: Function) -> Box<Fn(f64) -> f64>
    where Function: Fn(f64) -> f64
{
    Box::new(|x| x - g(x) / g.as_deriv()(x))
}

fn newtons_method<Function>(g: Function, guess: f64) -> f64
    where Function: Fn(f64) -> f64
{
    fixed_point(newton_transform(g), guess)
}

fn square_root(x: f64) -> f64 {
    newtons_method(|y| y * y - x, 1.0)
}

fn main() {
}
