package main

import ("fmt"; "math")

const small = 0.0000001

func isCloseEnough(v1, v2 float64) bool {
    return math.Abs(v1 - v2) < small
}

// it doesn't allow nested declarations

func fixed_point(f func(float64) float64, first_guess float64) float64 {
    // try_fp has to be declared in order to be seen inside its own body
    var try_fp func(float64) float64
    try_fp = func (guess float64) float64 {
        next := f(guess)

        if isCloseEnough(guess, next) {
            return next
        }

        return try_fp(next)
    }

    return try_fp(first_guess)
}

func deriv(g func(float64) float64) func(float64) float64 {
    dx := small
    return func(x float64) float64 {
        return (g(x + dx) - g(x)) / dx
    }
}

func newton_transform(g func(float64) float64) func(float64) float64 {
    return func(x float64) float64 {
        return x - g(x) / deriv(g)(x)
    }
}

func newtons_method(g func(float64) float64, guess float64) float64 {
    return fixed_point(newton_transform(g), guess)
}

func square_root(x float64) float64 {
    return newtons_method(func(y float64) float64 {
        return y * y - x
    }, 1)
}

func main() {
    fmt.Println("Square root of 2")
    fmt.Println("math.Sqrt:", math.Sqrt(2))
    fmt.Println("square_root with newton's method:", square_root(2))
}
