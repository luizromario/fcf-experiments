class Newton {
    static double small = 0.0000001;

    interface Function {
        double call(double x);
    }

    static boolean isCloseEnough(double v1, double v2) {
        return Math.abs(v1 - v2) < small;
    }

    static double fixedPoint(Function f, double firstGuess) {
        double guess = f.call(firstGuess);

        for (;;) {
            double next = f.call(guess);
            if (isCloseEnough(guess, next)) {
                return next;
            }
            guess = next;
        }
    }

    static Function deriv(Function g) {
        return x -> (g.call(x + small) - g.call(x)) / small;
    }

    static Function newtonTransform(Function g) {
        return x -> x - g.call(x) / deriv(g).call(x);
    }

    static double newtonsMethod(Function g, double guess) {
        return fixedPoint(newtonTransform(g), guess);
    }

    static double squareRoot(double x) {
        return newtonsMethod(y -> y * y - x, 1);
    }

    public static void main(String[] args) {
        System.out.println(squareRoot(10));
        System.out.println(Math.sqrt(10));
    }
}
