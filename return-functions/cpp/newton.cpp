#include <cmath>
#include <cstdlib>
#include <iostream>
#include <functional>

constexpr const double small = 0.0000001;

bool is_close_enough(double v1, double v2)
{
    return fabs(v1 - v2) < small;
}

template <typename Function>
double fixed_point(Function f, double first_guess)
{
    // declaring try_fp with auto not work because the recursive call to
    // try_fp will happen before the deduction of the auto
    // auto try_fp = [&](double guess)
    std::function<double(double)> try_fp = [&](double guess)
    {
        double next = f(guess);
        return is_close_enough(guess, next)?
            next : try_fp(next);
    };

    // declaring try_fp _then_ assigning the lambda won't work either
    // because, unlike Go (where that could work), there's no way to
    // express the type of a lambda in advance

    return try_fp(first_guess);
}

template <typename Function>
auto deriv(Function g)
{
    double dx = small;
    return [=](double x)
    {
        return (g(x + dx) - g(x)) / dx;
    };
}

template <typename Function>
auto newton_transform(Function g)
{
    return [=](double x)
    {
        return x - g(x) / deriv(g)(x);
    };
}

template <typename Function>
double newtons_method(Function g, double guess)
{
    return fixed_point(newton_transform(g), guess);
}

double square_root(double x)
{
    return newtons_method([=](double y)
    {
        return y * y - x;
    }, 1);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
        return 1;

    double n = atof(argv[1]);
    std::cout << "Square root of " << n << "\n";
    std::cout << "sqrt (cmath): " << sqrt(n) << "\n";
    std::cout << "square_root with newton's method: " << square_root(n) << "\n";

    return 0;
}
