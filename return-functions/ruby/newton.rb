SMALL = 0.0000001

def close_enough?(v1, v2)
    (v1 - v2).abs < SMALL
end

# without Proc.new, blocks can only be passed as values with the special
# block notation to methods -- that is, you cannot, say, directly store
# a block in a variable or return it like in the following examples:
#
# try_fp = do
#     # ...
# end
#
# def method
#     do
#         # ...
#     end
# end

def fixed_point(first_guess, &f)
    try_fp = Proc.new do |guess|
        next_guess = f.call guess
        if close_enough?(guess, next_guess)
            next_guess
        else
            try_fp.call next_guess
        end
    end

    try_fp.call first_guess
end

def deriv(&g)
    dx = SMALL
    Proc.new do |x|
        (g.call(x + dx) - g.call(x)) / dx
    end
end

def newton_transform(&g)
    Proc.new do |x|
        x - g.call(x) / deriv(&g).call(x)
    end
end

def newtons_method(guess, &g)
    fixed_point(guess, &newton_transform(&g))
end

def square_root(x)
    newtons_method(1){|y| y * y - x}
end
