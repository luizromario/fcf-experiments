#include <iostream>

template <typename Ret, typename... Args>
auto to_function_object(Ret (*function)(Args...))
{
    return [=](Args... a){ return function(a...); };
}

template <typename Class, typename Ret, typename... Args>
auto to_function_object(Class *this_ptr, Ret (Class::*member_function)(Args...))
{
    return [=](Args... a){ return (this_ptr->*member_function)(a...); };
}

int sum(int x, int y) { return x + y; }

struct Point
{
    float x = 0.0;
    float y = 0.0;

    Point sum(const Point& other) {
        return {x + other.x, y + other.y};
    }
};

template <typename F, typename... T>
void call_functionoid(F f, T... v)
{
    std::cout << f(v...) << "\n";
}

template <typename Stream>
Stream& operator<<(Stream& s, const Point& p)
{
    return s << "Point (" << p.x << ", " << p.y << ")";
}
    
int main()
{
    std::cout
        << "Convert function to function object: "
        << to_function_object(sum)(10, 20) << "\n";

    Point p1{10, 20};
    Point p2{30, 40};
    std::cout
        << "Convert member function to function object: "
        << to_function_object(&p1, &Point::sum)(p2) << "\n";

    // Now passing functions to a template that expects a function
    call_functionoid(sum, 10, 20);
    call_functionoid(to_function_object(sum), 10, 20);
    call_functionoid(to_function_object(&p1, &Point::sum), p2);
}
